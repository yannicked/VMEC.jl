# VMEC.jl

[![Stable](https://img.shields.io/badge/docs-dev-blue.svg)](https://wistell.gitlab.io/VMEC.jl/gitlab_pages)
[![Build Status](https://gitlab.com/wistell/VMEC.jl/badges/master/pipeline.svg)](https://gitlab.com/wistell/VMEC.jl/pipelines)
[![Coverage](https://gitlab.com/wistell/VMEC.jl/badges/master/coverage.svg)](https://gitlab.com/wistell/VMEC.jl/commits/master)

`VMEC.jl` is a Julia package for interfacing with the **V**ariational **M**oments **E**quilibrium **C**ode [(VMEC)](https://github.com/ORNL-Fusion/PARVMEC), a Fortran 77/90 program originally written by Hirshman *et al.*[^1] for three-dimensional ideal magnetohydrodynamic (MHD) equilibrium calculations.
This package includes methods for defining inputs for and running a VMEC calculation and methods and data structures to examine and use the output of VMEC calculations in the Julia REPL and other Julia functions.

## Installation
`VMEC.jl` is available through the Julia package manager (by pressing `]` in the REPL):
```julia
(@v1.7) > add VMEC
julia> using VMEC
```
The Fortran source code is packaged as a binary through the [`VMEC_jll.jl`](https://github.com/JuliaBinaryWrappers/VMEC_jll.jl) package.
While the core routines in `VMEC_jll.jl` are unchanged, the [source code](https://gitlab.com/wistell/VMEC2000/-/tree/v1.0.3) for the binary has been modified to allow for in-memory interfacing with Julia.
As such, VMEC can called directly from Julia to produce new equilibria, either through the REPL or as part of other Julia programs.

> **Caveat:** The ability to run VMEC from Julia is architecture dependent.
> Currently running VMEC from Julia is only available on x86-64 architectures running either Linux or MacOS.  
> Due the dependence of `VMEC_jll.jl` on other binaries, specifically the binary for [ScaLAPACK](https://www.netlib.org/scalapack/), `VMEC_jll.jl` is currently unavailable on Windows.
> A `VMEC_jll.jl` binary artifact is built for Windows, however it does not contain the VMEC source code.
> As soon as the appropriate Windows binaries become available, the Windows `VMEC_jll.jl` artifact will include the Fortran routines.

## Usage
The `VMEC.jl` package has routines and interfaces that can be used to:
  - Run a VMEC equilibrium calculation and store the results
  - Load the output from an existing VMEC equilibrium
  - Calculate the value of equilibrium magnetic field and geometry quantities at any point in the simulation volume.
The output of a VMEC calculation can then be used by other routines requiring geometry information data, see for example the [``\Gamma_c``](https://wistell.gitlab.io/GammaC.jl) calculation for energetic particle transport and the [PlasmaTurbulenceSaturationModel.jl](https://bfaber.gitlab.io/PlasmaTurbulenceSaturationModel.jl) for turbulence saturation calculations.
See the [Documentation](https://wistell.gitlab.io/VMEC.jl) for more details.

[^1]: [S. P. Hirshman and J. C. Whitson, Phys. Fluids 26, 3553 (1983)](https://aip.scitation.org/doi/10.1063/1.864116)
