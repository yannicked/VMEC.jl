# Loading VMEC output
VMEC data can be both loaded from a NetCDF file from a previous VMEC calculation or extracted from VMEC calculation using routines from the [Fortran Interface](@ref Fortran_public).
To load a VMEC equilibrium, one simply calls the `readVmecWout` function:
```julia
julia> vmec, vmec_data = readVmecWout(wout_file)
```
`wout_file` can either be a `String` or a NetCDF `NcFile` that has already been opened with `NetCDF.open`.

## Output data structures
Both methods for loading VMEC output data will result in the same data structures being constructed to hold the data, a [`Vmec`](@ref) type and a [`VmecData`](@ref) type.
Both the `VmecData` and `Vmec` types are subtypes of the `AbstractMagneticEquilibirum` type defined by the [PlasmaEquilibriumToolkit.jl](https://gitlab.com/wistell/PlasmaEquilibriumToolkit.jl) package automatically allowing these types to extend the methods defined by the `PlasmaEquilibriumToolkit`.
  - `VmecData <: AbstractMagneticEquilibrium `: Holds the raw data from a VMEC calculation or VMEC output file.
    The fields in `VmecData` are in one-to-one correspondence with the associated Fortran data structures (i.e. size and indexing).
    In particular, for spectral field data where the value of the Fourier mode amplitudes are stored for each surface, the underlying arrays will have dimension ``MN_{max} \times N_s``, where ``MN_{max}`` is the maximum number of independent Fourier modes and ``N_s`` is the number of surfaces in the VMEC calclulation.
```julia
using VMEC
fieldnames(VmecData) 

# output

(:rmnc, :rmns, :zmnc, :zmns, :lmnc, :lmns, :bmnc, :bmns, :gmnc, :gmns, :bsubsmnc, :bsubsmns, :bsubumnc, :bsubumns, :bsubvmnc, :bsubvmns, :bsupumnc, :bsupumns, :bsupvmnc, :bsupvmns, :currumnc, :currumns, :currvmnc, :currvmns, :xm, :xn, :xm_nyq, :xn_nyq, :phi, :phips, :phipf, :chi, :chipf, :iotaf, :iotas, :pres, :presf, :beta_vol, :raxis_cc, :raxis_cs, :zaxis_cs, :zaxis_cc, :am, :am_aux_s, :am_aux_f, :ac, :ac_aux_s, :ac_aux_f, :ai, :ai_aux_s, :ai_aux_f, :jcuru, :jcurv, :mass, :buco, :bvco, :vp, :specw, :over_r, :jdotb, :bdotb, :bdotgradv, :DMerc, :DShear, :DWell, :DCurr, :DGeod, :equif, :Aminor_p, :Rmajor_p, :aspect, :volume_p, :wb, :wp, :gamma, :rmax_surf, :rmin_surf, :zmax_surf, :betatotal, :betator, :betapol, :betaxis, :b0, :rbtor0, :rbtor, :IonLarmor, :volavgB, :mnmax, :mnmax_nyq, :ns, :mpol, :ntor, :signgs, :nfp, :pcurr_type, :piota_type, :pmass_type, :lasym, :lfreeb, :filename_nc)
```

  - `Vmec <: AbstractMagneticEquilibrium`: The `Vmec` type is derived from `VmecData`, but the radial dependence of any quantity has been interpolated across the plasma radius in the ``s`` coordinate using cubic B-spline intepolation from the [Interpolations.jl](https://github.com/JuliaMath/Interpolations.jl) package.
    For spectral quantities, this results in each independent mode being interpolated in ``s``.
    To enable faster evaluation of real space quantities by the methods defined in [Fourier Transform Utilities](@ref Fourier_public), the individual arrays for ``\sin`` and ``\cos`` amplitudes of a particular field have been combined into [VmecFourierSpline](@ref) datatype containing both the interpolation objects for the ``\sin`` and ``\cos`` amplitudes.
    For stellarator symmetric equilibria where each field is defined by only a ``\sin`` or ``\cos`` Fourier series, the amplitudes for the asymmetric series components are set to zero.

### Example
To illustrate, load the `"test/wout_aten.nc"` file for the precomputed ATEN equilibrium.
```julia
julia> using VMEC

julia> vmec, vmec_data = readVmecData("test/wout_aten.nc");

julia> @info "ns: $(vmec.ns), mnmax: $(vmec.mnmax), mnmnax_nyq: $(vmec.mnmax_nyq)"
[ Info: ns: 51, mnmax: 128, mnmax_nyq: 128

# vmec_data.rmnc is a regular 2D array with size (mnmax x ns)
julia> @info "$(typeof(vmec_data.rmnc)), $(size(vmec_data.rmnc))"
[ Info: Matrix{Float64}, (128, 51)

# vmec.rmn is a vector of VmecFourierSplines with size (128,)
julia> @info "$(typeof(vmec_data.rmnc)), $(size(vmec_data.rmnc))"
[ Info: Vector{VmecFourerSpline{Float64}}, (128,)
```
From this example example, we see the two objects created from [`readVmecWout`](@ref): `vmec_data::VmecData` and `vmec::Vmec`.
The `vmec_data` object has the arrays `rmnc` and `rmns` with the mode aplitudes of each independent ``R^c_{m,n}`` and ``R^s_{m,n}`` mode at each of the `ns` surfaces.
The `vmec` object only has the field `rmn`, a vector of [`VmecFourierSpline`](@ref) objects for *both* the ``\sin`` and ``\cos`` components for each independent mode number.
In addition to containing the ``\cos`` and ``\sin`` cubic B-spline interpolations for the mode, the `VmecFourierSpline` type also holds the poloidal mode number `m` and the toroidal mode number `n`. 

