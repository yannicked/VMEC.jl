# Private API

## Types
```@autodocs
Modules = [VMEC]
Public = false
Pages = ["VmecTypes.jl"]
```

## Fortran Interface
```@autodocs
Modules = [VMEC]
Public = false
Pages = ["FortranInterface.jl", "NamelistInput.jl", "NetCDFUtils.jl"]
```

## Fourier Transform Utilities
```@autodocs
Modules = [VMEC]
Public = false
Pages = ["FourierTransforms.jl"]
```

## Coordinate Transformations
```@autodocs
Modules = [VMEC]
Public = false
Pages = ["Coordinates.jl", "Boozer.jl"]
```
