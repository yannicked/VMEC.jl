# VMEC\_jll

[`VMEC_jll.jl`](https://github.com/JuliaBinaryWrappers/VMEC_jll.jl) is the binary library wrapper for the original Fortran VMEC source code.
New interfaces have been added to allow for the in-memory setting/getting of VMEC input/output variables, as well as initializing/finalizing and running a VMEC calculation.
The subroutines exposed by `VMEC_jll.libvmec_*` are not intended to be called by users, but are instead handled by Julia functions defined in [Fortran Interface](@ref).
`VMEC_jll` builds two different libraries for VMEC: `libvmec_openblas` which depends on the OpenBLAS/ScaLAPACK libraries and `libvmec_mkl` which uses the BLAS/ScaLAPACK routines from in Intel Math Kernel Library (MKL).
For Intel chipsets, the MKL libraries tend to provide better performance; on other x86-64 architectures the OpenBLAS/ScaLAPACK libraries are used.
The user does not need to have these libraries installed on their system, thanks to the Julia package manager, binary artifacts for these libraries will be automatically downloaded in used.

Upon initiation of the VMEC module, a check of the operating system and chipset is performed to determine the best VMEC library to load, see the code below from `src/VMEC.jl`:
```julia
const libvmec_ref = Ref{String}("")
const libvmec_dlopened = Ref{Bool}(false)
function __init__()
    if Sys.iswindows()
        @debug "ScaLAPACK is not available on Windows, running VMEC is not possible"
    else
        if occursin("Intel",first(Sys.cpu_info()).model)
            libvmec_ref[] = dlopen(libvmec_mkl; throw_error = false) !== nothing ? libvmec_mkl : libvmec_openblas
        else
            libvmec_ref[] = libvmec_openblas
        end
    end
    try
      ccall((:__vmec_ext_interface_MOD_initialize, libvmec_ref[]), Nothing, (Ptr{Int32}, Ptr{Int32}), [4], [4])
      ccall((:__vmec_ext_interface_MOD_finalize, libvmec_ref[]), Nothing, ())
      libvmec_dlopened[] = true
    catch
      @debug "Unable to call VMEC initialize/finalize, running VMEC is not possible"
    end
end
```
If a dynamically loadable `libvmec` is found and able to be initialized and finalized, the appropriate `libvmec` value is set and the `libvmec_dlopened` flag is set to `true`.
For systems where `libvmec` has not yet been built, the `libvmec_dlopened` flag is set to `false` and any attempts to call the VMEC interface routines in [Fortran Interface](@ref) will emit a warning and return `nothing`.

To set/get internal VMEC variables, subroutines have been implemented to access the data by passing the Julia variable symbol to initiate a request.
If a user is seeking set a variable with canonical name "`fieldname`" (represented in the Julia namelist dictionary with the key `:fieldname`) that does not yet have a Fortran interface defined, they will encounter the following error message:
```
Fatal VMEC input error!
No data with label <fieldname> found! 
```
Similarly, if a user wants to retrieve `fieldname` but no interface exists, the following error message will be returned:
```
No data field with label <fieldname> found!
```
If a user would like an interface defined, submit a feature request on this [Issues page](https://gitlab.com/wistell/VMEC2000/-/issues).
