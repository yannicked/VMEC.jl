# Running VMEC from Julia

## Setting up a VMEC calculation
VMEC input files written in the Fortran namelist format are read through the `VMEC.readVmecNamelist` function and returns an `OrderedDict` where the keys are the symbols corresponding to the parameter names and the values are the input parameter values.
We can, for example, load a VMEC namelist using the input file for the "ATEN" equilibrium[^1] found at `test/input.aten`:

```julia
julia> using VMEC

julia> aten_input_path = "test/input.aten";

julia> nml = VMEC.readVmecNamelist(aten_input_path)
OrderedCollections.OrderedDict{Symbol, Any} with 65 entries:
  :delt               => 0.8
  :nstep              => 200
  :tcon0              => 2.0
  :ns_array           => [51, 51]
  :niter_array        => [10000, 10000]
  :ftol_array         => [1.0e-7, 1.0e-10]
  :precon_type        => "none"
  :prec2d_threshold   => 1.0e-19
  :lasym              => false
  :nfp                => 4
  :mpol               => 8
  :ntor               => 8
  :phiedge            => 0.4444
  :lfreeb             => false
  :mgrid_file         => ""
  :nzeta              => 50
  :nvacskip           => 6
  :gamma              => 0.0
  :bloat              => 1.0
  :spres_ped          => 1.0
  :pres_scale         => 0.0
  :pmass_type         => "power_series"
  :am                 => [1, -1, 0, 0, 0, -1, 1]
  :curtor             => 0.0
  :ncurr              => 1
  :pcurr_type         => "sum_atan"
  :ac                 => [0.0, 1.0, 10.0424, 1.50747, 1.0, 0.0, 0.0, 0.0, 0.0]
  :raxis              => [2.01841, 0.405698, 0.023376, 0.0080961]
  :zaxis              => [0.0, -0.298721, -0.0240989, -0.00491058]
  Symbol("rbc(0,0)")  => 2
  Symbol("zbs(0,0)")  => 0
  Symbol("rbc(1,0)")  => 0.322516
  Symbol("zbs(1,0)")  => -0.192973
  Symbol("rbc(2,0)")  => 0.0339026
  Symbol("zbs(2,0)")  => -0.032571
  Symbol("rbc(3,0)")  => 0.00444731
  Symbol("zbs(3,0)")  => -0.00448874
  Symbol("rbc(-2,1)") => -0.00704736
  Symbol("zbs(-2,1)") => -0.00710084
  Symbol("rbc(-1,1)") => 0.00798449
  Symbol("zbs(-1,1)") => 0.00842841
  Symbol("rbc(0,1)")  => 0.313169
  Symbol("zbs(0,1)")  => 0.353465
  Symbol("rbc(1,1)")  => -0.137359
  Symbol("zbs(1,1)")  => 0.137914
  Symbol("rbc(2,1)")  => -0.0349872
  Symbol("zbs(2,1)")  => 0.0349564
  Symbol("rbc(3,1)")  => 0.00119705
  Symbol("zbs(3,1)")  => -0.0012374
  Symbol("rbc(-1,2)") => -0.00349467
  Symbol("zbs(-1,2)") => -0.00464158
  Symbol("rbc(0,2)")  => 0.0305112
  Symbol("zbs(0,2)")  => 0.0440111
  Symbol("rbc(1,2)")  => 0.016655
  Symbol("zbs(1,2)")  => 0.061206
  Symbol("rbc(2,2)")  => 0.0533023
  Symbol("zbs(2,2)")  => -0.0527733
  Symbol("rbc(3,2)")  => -0.006042
  Symbol("zbs(3,2)")  => 0.00191759
  Symbol("rbc(0,3)")  => 0.00193322
  Symbol("zbs(0,3)")  => 0.00426693
  Symbol("rbc(1,3)")  => 0.00387607
  Symbol("zbs(1,3)")  => -0.0108801
  Symbol("rbc(2,3)")  => 0.000172233
  Symbol("zbs(2,3)")  => 0.00916101
```
Note that for the specification of the ``R`` and ``Z`` boundary coefficients, the `String`-type keywords present in the input files are converted to `Symbol` types.
A list of all of the standard VMEC input keywords can be found on this [STELLOPT/VMEC documentation page](https://princetonuniversity.github.io/STELLOPT/VMEC%20Input%20Namelist%20(v8.47)).

> **Caveat:** Not every input parameter listed on that page is accessible for modification.
> In particular, parameters corresponding only to equilibrium reconstruction are not yet available.
> See the page on [VMEC_jll](@ref) for more information. 

A key benefit of this approach to storing VMEC input data is that input values can be modified, new entries added, or unwanted entries deleted interactively and without having to write a new input file.  

## Running a VMEC calculation
With a namelist dictionary defined, running VMEC is quite simple, one calls [`VMEC.runVmec`](@ref). 
VMEC requires an MPI communicator to execute, however if one is not provided, `MPI.COMM_WORLD` will be used as default.
The using the `test/input.aten` file, the following code snippet demonstrates the required steps to run VMEC:
```julia
julia> using VMEC

julia> nml = VMEC.readVmecNamelist("input.aten");

julia> vmec, vmec_data = VMEC.runVmec(nml);
```
This will execute VMEC using one MPI process.

To run VMEC on more than one MPI process, one can follow the following script (which could be saved to the file `run_vmec.jl`):
```julia
using MPI, VMEC

MPI.Init()
rank = MPI.Comm_rank(MPI.COMM_WORLD)
# Read just on head node
nml = rank == 0 ? VMEC.readVmecNamelist("input.aten")  : nothing
if MPI.Comm_size(MPI.COMM_WORLD) > 1
  nml = MPI.bcast(nml, 0, MPI.COMM_WORLD)
end
vmec, vmec_data = VMEC.runVmec(MPI.COMM_WORLD, nml)
```
To run this on `N` processes, use the standard procedure for running MPI:
```
mpiexec -n N julia run_vmec.jl
```
or by using the [Julia-specific `mpiexecjl` wrapper](https://juliaparallel.github.io/MPI.jl/stable/configuration/#Julia-wrapper-for-mpiexec):
```
mpiexecjl -n N run_vmec.jl
```
Calls to `VMEC.runVmec` will return a [`Vmec`](@ref) and [`VmecData`](@ref) object containing the results of the calculation, see [Loading VMEC output](@ref) for more details.

[^1]: [Bader, A. *et al* Advancing the physics basis for quasi-helically symmetric stellarators. Journal of Plasma Physics, 86(5), 905860506 (2020)](https://doi.org/10.1017/S0022377820000963)
