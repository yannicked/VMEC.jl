# Working with VMEC data
The primary operation performed with data from a VMEC equilibrium is to determine the value of different physical quantities, such as the vector magnetic field ``\mathbf{B}``, in different curvilinear coordinate representations, such as the [`PEST`](https://arxiv.org/pdf/1904.01682v2.pdf) or [`Boozer`](https://doi.org/10.1063/1.863297) coordinates.
The `VMEC.jl` package extends the methods of the [PlasmaEquilibriumToolkit.jl](https://wistell.gitlab.io/PlasmaEquilibriumToolkit.jl/dev) by providing definitions for methods in the `PlasmaEquilibriumToolkit` API specialized to the `Vmec` and [`VmecSurface`](@ref) types.

## 1D radial data
For VMEC data has only radial dependence, the `Vmec` datatype records this data as a cubic B-spline in the ``s`` surface label coordinate.
For example, the rotational transform ``\iota(s)`` or the differential change in volume ``V'`` are represented through cubic B-splines.
```julia
julia> using VMEC

julia> vmec, vmec_data = readVmecWout("test/wout_aten.nc");

julia> s = pi/10;

julia> vmec.iota(s)
1.0893535852820655

julia> vmec.vp(s)
0.08999613170369762
```
We can use the `Interpolations.gradient` function to compute the derivative of any VMEC data with respect to ``s``.
```julia
julia> Interpolations.gradient(vmec.iota, s)[]
0.026829681441634112
```
We can use this functionality to easily compute the magnetic well/hill term defined as ``-\frac{d V'}{ds}`` at any point across the plasma minor radius (requiring the sign of the Jacobian for consistency):
```julia
julia> map(s -> -vmec.signgs * Interpolations.gradient(vmec.vp, s)[], 0.1:0.1:0.9)
9-element Vector{Float64}:
 -0.001973300836360589
 -0.001479813716523306
 -0.0009862013379938889
 -0.0004867408964778254
  5.31326251272321e-5
  0.0006532708764130785
  0.0013368208959745054
  0.00213139838693803
  0.0030781302031483715
```
This procedure can be used to find the value and derivative of any VMEC quantity with radial dependence at any location for ``s \in (0,1)``.  

## 2D surface data
A common application requiring VMEC geometry information is the calculation of various quantities of interest, such as vector magnetic field ``\mathbf{B}`` or the normal magnetic curvature ``\kappa_n`` for example, local to a magnetic field line or surface.
As VMEC assumes nested flux surfaces, magnetic field lines are bound to a flux surface.
As is show in the [Theory](@ref) section, most quantities of interest are represented by Fourier summations in the *internal VMEC* coordinate system, ``A(\theta_v, \zeta_v) = \sum\limits_{m, n} \tilde{A}_{m, n} e^{\mathrm{i} m \theta_v + \mathrm{i} n \zeta_v}`` where ``(\theta_v, \zeta_v)`` are the poloidal and toroidal angles used in the VMEC equilibrium calculation.
To enable a more efficient method for calculating the Fourier transforms, the [`VmecSurface`](@ref) type has been defined, which contains only the data at the surface desired.
In particular, the spectral content has been reduced from a spline representation to a `Vector{VmecFourierData}`, where the [`VmecFourierData`](@ref) type is an alias for a 6-element static vector that holds the poloidal and toroidal mode numbers, and the ``\cos``, ``d\cos/ds``, ``\sin``, ``d\sin/ds`` values evaluated at the desired surface.
The `VmecFourierData` type is used by [`Fourier Transform Utilites`](@ref Fourier_public) to efficiently calculate the sums involved with the modified Fourier transform.
Note that the `VmecFourierData` type also contains the derivative with respect to ``s`` of the mode amplitudes, this feature is replicated for any radially-dependent equilibrium quantity.

The fieldnames of the `VmecSurface` type have one-to-one correspondence with fieldnames of the `Vmec` type (except for the field `s` which denotes the surface label), however the content with the field is different.
For VMEC quantities with radial dependence, the information contained in the identically named `VmecSurface` field will always have the form of a static `SVector`, where the first index corresponds to the interpolating spline evaluated at the surface location and the second value is the derivative of the spline with respect to ``s`` evaluated at the surface location.
For example:
```julia
julia> using VMEC, Interpolations

julia> vmec, vmec_data = readVmecWout("test/wout_aten.nc");

julia> vmec_surface = VmecSurface(pi/10, vmec);

julia> @info "typeof(vmec.iota): $(typeof(vmec.iota)), typeof(vmec_surface.iota): $(typeof(vmec_surface.iota))"
] Info: CubicSplineInterpolation..., SVector{2, Float64}

julia> vmec.iota(pi/10) == vmec_surface.iota[1] && Interpolations.gradient(pi/10, vmec.iota) == vmec_surface.iota[2]
true
```

To obtain the values of geometry quantities defined by spectral representations on `VmecSurface`, the [`inverseTransform`] function is used to compute the value at a specific coordinate using the `VmecFourierData` structure.  
The current restriction on using the `inverseTransform` function is that it is only defined with `VmecCoordinates` as an argument.
Thus one must first convert from other coordinate systems to `VmecCoordinates` to properly use the `inverseTransform` function.
The `inverseTransform` function accepts the `deriv` keyword argument and can take on the `Symbol` values `:ds`, `:dθ`, `:dζ` (not the leading `:` is necessary), which returns the desired derivative of a field.
Currently only first-order derivatives have been implmented.
See the code below for an example: 
```julia
julia> using VMEC

julia> vmec, _ = readVmecWout("test/wout_aten.nc")

julia> vmec_surface = VmecSurface(0.5, vmec);

julia> x = PestCoordinates(0.5, 0.0, 0.0);

julia> v = VmecFromPest()(x, vmec_surface);

# Obtain the value of the R
julia> R = inverseTransform(v, vmec_surface.rmn)
2.5510291092432964

# Obtain the value of dR/ds
julia> dRds = inverseTransform(v, vmec_surface.rmn; deriv = :ds)
0.11818665327948297
```

## Integration with the `PlasmaEquilibriumToolkit`
In many cases, it is more desirable to use different coordinate systems, such as those provided by the `PlasmaEquilibriumToolkit`, for analysis.
This requires a change of basis from the desired coordinate system to internal VMEC coordinates, usually through an intermediate change of basis to straight fieldline coordinates to use the relation ``\theta = \theta_v + \lambda\left(\theta_v, \zeta_v \right)``, where ``\theta_v`` is the internal VMEC poloidal angle and ``\zeta_v`` is toroidal angle.
To do this seamlessly, `VMEC.jl` provides several coordinate transformations using the machinery of the `PlasmaEquilibriumToolkit` and will automatically determine the internal VMEC coordinates by finding the root of ``\theta = \theta_v + \lambda\left( \theta_v, \zeta_v \right)`` using a root finding method from [Roots.jl](https://github.com/JuliaMath/Roots.jl).
As was demonstrated in the previous example, the coordinate transformation from `PestCoordinates` to `VmecCoordinates` is facilitated by the `VmecSurface` type, holding the values of ``\lambda_{m, n}`` on the surface (as well as rotational transform ``\iota(s)``).
