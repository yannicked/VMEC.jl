# This script will show how to load a VMEC file using the Julia interface
# and how to query some quantities of interest
# The first section will load the vmec file and the remaining
# sections will do some basic manipulations of the data


# Set the vmec path. The user will need to change the path below
vmecPath = "/home/abader/VMEC.jl";
# Set the path for the plasma equilibrium toolkit
petPath = "/home/abader/PET.jl";

# Set the path to the vmec WOUT file.  The user will need to change the path below
woutPath = "/home/abader/runs/adelle/wout_RUN12B.00515.nc";

#Add the vmec path and the PET paths
push!(LOAD_PATH,vmecPath); 
push!(LOAD_PATH,petPath);

#load dependencies
using NetCDF; using VMEC; using PlasmaEquilibriumToolkit

#load in the raw VMEC data from NETCDF
wout = NetCDF.open(woutPath);

# load the VMEC data into two different formats
# "vmecdata" is a straightforward reconstruction of the vmec wout file but in a julia struct
# "vmec" is a processed form in which all relevant quantities are calculated as splines over the radial coordinate
vmec,vmecdata = VMEC.readVmecWout(wout);

#the stuff included up to this point is necessary for anything below
#the following sections, separated by dashed comment lines
#can be separated out individually and pasted into repl as desired

#--------------------------------------------------------
#Section 1: 
#Generate points along a field line, convert to cartesian and plot in 3D

#make an array of zeta points centered around 0
npoints = 500;
maxZeta = 6*π;

#note: the collect function turns an iterable into an array
zetas = collect(-maxZeta:2*maxZeta/npoints:maxZeta);

#set the s and alpha values
s = 0.8;
alpha = 0;

#generate pest coordinates
pestCoords = PestCoordinates(s, alpha, zetas);

#to do the transformation, we need the vmec information on the surface
vmecSurface = VmecSurface(s, vmec);

#transform to cartesian (go through vmec b/c cartesian from pest not there yet?)
vmecCoords = VmecFromPest()(pestCoords, vmecSurface)
cartCoords = CartesianFromVmec()(vmecCoords, vmecSurface)

println(cartCoords[1])

#ben might know a better way to do this
X = [q[1] for q in cartCoords]
Y = [q[2] for q in cartCoords]
Z = [q[3] for q in cartCoords]

#I'm not good enough at julia plotting to make this look good
using Plots
plot(X,Y,Z,aspect_ratio=:equal, legend=:none)

#--------------------------------------------------------
#Section 2:
#make a plot of the magnetic field strength along a field line

#make an array of zeta points centered around 0
npoints = 500;
maxZeta = 6*π;

#note: the collect function turns an iterable into an array
zetas = collect(-maxZeta:2*maxZeta/npoints:maxZeta);

#set the s and alpha values
s = 0.8;
alpha = 0;

#generate pest coordinates
pestCoords = PestCoordinates(s, alpha, zetas);

#to do the transformation, we need the vmec information on the surface
vmecSurface = VmecSurface(s, vmec);

#generate vmec coords
vmecCoords = VmecFromPest()(pestCoords, vmecSurface)

#calculate the magnetic field strength
Bmag = [inverseCosineTransform(v, vmecSurface.bmn) for v in vmecCoords]

plot(zetas,Bmag,linewidth=2,linecolor=:blue)
