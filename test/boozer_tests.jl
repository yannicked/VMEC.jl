@testset "VMEC Surface Tests" begin
  vmec = VMEC.readVmecWout(joinpath(@__DIR__, "wout_circular_tokamak.nc"));
  vmec46 = VMEC.readVmecWout(joinpath(@__DIR__, "wout_qhs46.nc"));
  s = 0.5
  vmecsurf = VMEC.VmecSurface(s, vmec)
  vmecsurf_s4 = VMEC.VmecSurface(0.4, vmec46)
  vmecsurf_s5 = VMEC.VmecSurface(0.5, vmec46)
  rtol = 1.0E-6
  ctol = 1.0E-4 #lower tolerance for coordinate transformation
  (wmn, g, i) = VMEC.periodic_boozer_function(vmecsurf)
  @testset "Boozer components" begin
    @test g == vmecsurf.bvco[1]
    @test i == vmecsurf.buco[1]
    for i in 1:vmecsurf.mnmax
      if i == 1
        @test wmn[i].sin == 0.0
      else
        @test wmn[i].sin == (vmecsurf.bsubumn[i].cos)/vmecsurf.xm[i]
      end
    end
  end
  @testset "Quasisymmetry Deviation" begin
    @test VMEC.quasisymmetry_deviation(1, 0, 48, 15, vmecsurf) == 0.0
    @test isapprox(VMEC.quasisymmetry_deviation(1, 1, 48, 15, vmecsurf), 0.23028045549994938,
                   rtol = rtol)
    #Test on qhs46, benchmarked against xbooz_xform
    qs_s4 = VMEC.quasisymmetry_deviation(1, 4, 48, 15, vmecsurf_s4)
    qs_s5 = VMEC.quasisymmetry_deviation(1, 4, 48, 15, vmecsurf_s5)
    @test isapprox(qs_s4, 0.01985313822501012, rtol = rtol)
    @test isapprox(qs_s5, 0.024667743911066072, rtol = rtol)
    @test VMEC.quasisymmetry_deviation(1, 4, 48, 15, [vmecsurf_s4, vmecsurf_s5]) == [qs_s4, qs_s5]
  end
  @testset "Coordinate transformations" begin
    vc = VmecCoordinates(0.5, 0.1, 0.2)
    bc = BoozerFromVmec()(vc, vmecsurf)
    vc_recalc = VmecFromBoozer()(bc, vmecsurf)
    @test isapprox(vc, vc_recalc, rtol=ctol)
    vc2 = VmecCoordinates(0.5, 0.2, 0.3)
    bc_array = BoozerFromVmec()([vc, vc2], vmecsurf)
    (vc_recalc, vc2_recalc) = VmecFromBoozer()(bc_array, vmecsurf)
    @test isapprox(vc, vc_recalc, rtol=ctol)
    @test isapprox(vc2, vc2_recalc, rtol=ctol)

  end
  boozsurf = BoozerSurface(vmecsurf_s5, 48, 15)
  @testset "Boozer Surface Calculation" begin
    rmn = VMEC.boozer_fourier_spectrum(:rmn, vmecsurf_s5, 48, 15)
    @test rmn == boozsurf.rmn
    zmn = VMEC.boozer_fourier_spectrum(:zmn, vmecsurf_s5, 48, 15)
    @test zmn == boozsurf.zmn
    gmn = VMEC.boozer_fourier_spectrum(:gmn, vmecsurf_s5, 48, 15)
    @test gmn == boozsurf.gmn
    bmn = VMEC.boozer_fourier_spectrum(:bmn, vmecsurf_s5, 48, 15)
    @test bmn == boozsurf.bmn
    νmn = VMEC.boozer_fourier_spectrum(:νmn, vmecsurf_s5, 48, 15)
    @test νmn == boozsurf.νmn
    λmn = VMEC.boozer_fourier_spectrum(:λmn, vmecsurf_s5, 48, 15)
    @test λmn == boozsurf.λmn
  end
  @testset "Coordinate Transformation BoozerSurface" begin
    vc = VmecCoordinates(0.5, 0.1, 0.2)
    bc = BoozerFromVmec()(vc, vmecsurf_s5)
    vc_recalc = VmecFromBoozer()(bc, boozsurf)
    @test isapprox(vc, vc_recalc, rtol=ctol)
    vc2 = VmecCoordinates(0.5, 0.2, 0.3)
    bc_array = BoozerFromVmec()([vc, vc2], vmecsurf_s5)
    (vc_recalc, vc2_recalc) = VmecFromBoozer()(bc_array, boozsurf)
    @test isapprox(vc, vc_recalc, rtol=ctol)
    @test isapprox(vc2, vc2_recalc, rtol=ctol)
    cc_vmec = CylindricalFromBoozer()(bc, vmecsurf_s5)
    cc_booz = CylindricalFromBoozer()(bc, boozsurf)
    @test isapprox(cc_vmec, cc_booz, rtol=ctol)
    (cc_vmec1, cc_vmec2) = CylindricalFromBoozer()(bc_array, vmecsurf_s5)
    (cc_booz1, cc_booz2) = CylindricalFromBoozer()(bc_array, boozsurf)
    @test isapprox(cc_vmec1, cc_booz1, rtol=ctol)
    @test isapprox(cc_vmec2, cc_booz2, rtol=ctol)
  end
end
