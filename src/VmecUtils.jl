
"""
    thetaVmec(x::FluxCoordinates,Λmn::FourierData,interval=0.25)

Computes the internval VMEC θ coordinate associated with the points `x` specified
in `FluxCoordinates`, `Λmn` must be the `FourierData` from the surface
specified by `x.ψ`.
"""
function thetaVmec(x::FluxCoordinates,
                  Λmn::AbstractVector{SurfaceFourierData{T}},
                  interval=0.25) where {T <: AbstractFloat}
  function residual(θ::T)
    return θ - x.θ + inverseTransform(FluxCoordinates(x.ψ, θ, x.ζ), Λmn)
  end

  bracket = (x.θ-interval,x.θ+interval)
  try
    return Roots.find_zero(residual,bracket,Roots.Order2())
  catch err
    if is(err,Roots.ConvergenceFailed) && attempt <= 5
      return thetaVmec(x,Λmn,2*interval)
    end
  end
end


function thetaVmec(x::FluxCoordinates,
                   vmec::VmecSurface;
                  )
  return thetaVmec(x, vmec.λmn)
end

function thetaVmec(x::AbstractArray{FluxCoordinates},
                   vmec::VmecSurface{T};
                  ) where {T <: AbstractFloat}
  y = Array{T, ndims(x)}(undef,size(x))
  @batch for i in eachindex(x)
    y[i] = thetaVmec(x[i], vmec.λmn)
  end
  return y
end

function PlasmaEquilibriumToolkit.grad_B(x::VmecCoordinates,
                                         vmec::VmecSurface{T};
                                        ) where {T}
  e_x = basis_vectors(Covariant(), CartesianFromVmec(), x, vmec)
  return grad_B(x, e_x, vmec)
end

function PlasmaEquilibriumToolkit.grad_B(x::PestCoordinates,
                                         vmec::VmecSurface{T};
                                        ) where {T}
  y = VmecFromPest()(x, vmec)
  return grad_B(y, vmec)
end

function PlasmaEquilibriumToolkit.grad_B(x::FluxCoordinates,
                                         vmec::VmecSurface{T};
                                        ) where {T}
y = VmecFromFlux()(x, vmec)
return grad_B(y, vmec)
end

function PlasmaEquilibriumToolkit.grad_B(x::VmecCoordinates,
                                         ev::BasisVectors{T},
                                         vmec::VmecSurface{T};
                                        ) where {T}
  dBds = inverseTransform(x,vmec.bmn;deriv=:ds)
  dBdθ = inverseTransform(x,vmec.bmn;deriv=:dθ)
  dBdζ = inverseTransform(x,vmec.bmn;deriv=:dζ)
  return ev[:,1]*dBds .+ ev[:,2]*dBdθ .+ ev[:,3]*dBdζ
end

function PlasmaEquilibriumToolkit.grad_B(x::PestCoordinates,
                                         ev::BasisVectors{T},
                                         vmec::VmecSurface{T};
                                        ) where {T}
    y = VmecFromPest()(x, vmec)
    return grad_B(y, ev, vmec)
end

function PlasmaEquilibriumToolkit.grad_B(x::FluxCoordinates,
                                         ev::BasisVectors{T},
                                         vmec::VmecSurface{T};
                                        ) where {T}
    y = VmecFromFlux()(x, vmec)
    return grad_B(y, ev, vmec)
end

function PlasmaEquilibriumToolkit.B_norm(x::VmecCoordinates,
                                         vmec::VmecSurface{T};
                                        ) where {T}
  return inverseTransform(x,vmec.bmn)
end

function PlasmaEquilibriumToolkit.B_norm(x::PestCoordinates,
                                         vmec::VmecSurface{T};
                                        ) where {T}
    b = B_field(x, vmec)
    return norm(b, 2)
end

function PlasmaEquilibriumToolkit.B_norm(x::FluxCoordinates,
                                         vmec::VmecSurface{T};
                                        ) where {T}
    b = B_field(x, vmec)
    return norm(b, 2)
end

function PlasmaEquilibriumToolkit.B_field(x::VmecCoordinates,
                                          vmec::VmecSurface{T};
                                         ) where {T}
  ∇x = basis_vectors(Contravariant(), CartesianFromVmec(), x, vmec)
  return B_field(Contravariant(), ∇x)
end

function PlasmaEquilibriumToolkit.B_field(x::PestCoordinates,
                                          vmec::VmecSurface{T};
                                         ) where {T}
    ∇x = basis_vectors(Contravariant(), CartesianFromPest(), x, vmec)
    return cross(∇x[:, 1], ∇x[:, 2])
end

function PlasmaEquilibriumToolkit.B_field(x::FluxCoordinates,
                                          vmec::VmecSurface{T};
                                         ) where {T}
    ∇x = basis_vectors(Contravariant(), CartesianFromFlux(), x, vmec)
    return cross(∇x[:,1], ∇x[:,2] - vmec.iota[1] * ∇x[:,3])
end

function PlasmaEquilibriumToolkit.jacobian(x::VmecCoordinates,
                                           vmec::VmecSurface{T};
                                          ) where {T}
  return inverseTransform(x, vmec.gmn)
end

function PlasmaEquilibriumToolkit.jacobian(x::PestCoordinates,
                                           vmec::VmecSurface{T};
                                          ) where {T}
    ∇x = basis_vectors(Contravariant(), CartesianFromPest(), x, vmec)
    return jacobian(Contravariant(), ∇x)
end

function PlasmaEquilibriumToolkit.jacobian(x::FluxCoordinates,
                                           vmec::VmecSurface{T};
                                          ) where {T}
    ∇x = basis_vectors(Contravariant(), CartesianFromFlux(), x, vmec)
    return jacobian(Contravariant(), ∇x)
end
