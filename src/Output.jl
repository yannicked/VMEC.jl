using HDF5

function writeEqH5(vmec::VmecData,filename::String)
  fid = h5open(filename,"w")
  g_create(fid,"vmec")
  gid = fid["vmec"]
  entries = propertynames(vmec)
  for entry in entries
    stringId = String(entry)
    try
      data = getfield(vmec,entry)
      write(gid,stringId,data)
    catch
    end
  end
  close(fid)
end

"""
    sanitizeString(s::AbstractString)

Return the appropriate string values for use in a Fortran namelist
"""
function sanitizeString(s::AbstractString)
  #check for [ characters
  if occursin(r"\[",s)
    s = replace(s,"["=>"")
    s = replace(s,"]"=>"")
  end
  if occursin(r",",s)
    s = replace(s,","=>"")
  end
  #check for empty strings
  if length(s) == 0
    return "''"
  end
  #check for strings
  if isletter(s[1])
    if s == "false"
      return "F"
    elseif s == "true"
      return "T"
    else
      return "'"*s*"'"
    end
  end

  return s
end

"""
    writeVmecInput(namelistDict::AbstractDict{Symbol,Any}, filename::String)

Write a dictionary representing the VMEC input to a traditional Fortran namelist
file labeled by `filename`, allowing for independent calculation of the VMEC.

See also: [`readVmecNamelist`](@ref)
"""
function writeVmecInput(namelistDict::AbstractDict{Symbol,Any},
                        filename::String;
                       )
    fid = open(filename, "w")
    write(fid,"&INDATA\n")
    for (key, value) in namelistDict
        write(fid,"  ")
        write(fid,key)
        write(fid," = ")
        newString = sanitizeString(string(value))
        write(fid,newString)
        write(fid,"\n")
        #end
    end
    write(fid,"/\n")

    close(fid)
end

function geneGeometryFile(woutfile::String,
                          s0::T,
                          alpha0::Union{T,Vector{T}},
                          nz0::Int,
                          pol_turns::Int,
                          filename::String;
                         ) where T
    wout = NetCDF.open(woutfile)
    vmec = readVmecWout(wout)
    geneGeometryFile(vmec, s0, alpha0, nz0, pol_turns, filename)
end

function geneGeometryFile(vmec::Vmec,
                          s0::T,
                          α₀::Union{T,Vector{T}},
                          nz0::Int,
                          pol_turns::Int,
                          filename::String;
                            ) where T
    vmec_surf = VmecSurface(s0,vmec)

    # VMEC ι is left-handed, PEST ι is right handed
    ι = -vmec_surf.iota[1]

    ζ_range = range(α₀-pol_turns*π/ι,step=2*π*pol_turns/nz0/ι,length=nz0)

    points = MagneticCoordinateCurve(PestCoordinates,vmec_surf.phi[end]*s0*vmec_surf.signgs/(2π),α₀,ζ_range)

    metric, modB, sqrtg, K1, K2, ∂B = geneGeometryCoefficients(GeneFromPest(),points,vmec_surf)
    PlasmaEquilibriumToolkit.writeGeneGeometry(filename,points,vmec_surf,metric,modB,sqrtg,K1,K2,∂B)
end

function geneGeometryFilePoloidal(vmec::Vmec,
                          s0::T,
                          α₀::Union{T,Vector{T}},
                          nz0::Int,
                          pol_turns::Int,
                          filename::String;
                            ) where T
    s0_pol = PoloidalVmecFromVmec()(VmecCoordinates(s0,0,0), vmec).s
    vmec_surf = VmecSurface(s0_pol,vmec)

    # VMEC ι is left-handed, PEST ι is right handed
    ι = -vmec_surf.iota[1]

    ζ_range = range(α₀-pol_turns*π/ι,step=2*π*pol_turns/nz0/ι,length=nz0)

    points_pesp = MagneticCoordinateCurve(PespCoordinates,vmec_surf.chi[end]*s0_pol*vmec_surf.signgs/(2π),α₀,ζ_range)

    metric, modB, sqrtg, K1, K2, ∂B = geneGeometryCoefficients(GeneFromPesp(),points_pesp,vmec_surf)

    points_pest = MagneticCoordinateCurve(PespCoordinates,vmec_surf.phi[end]*s0*vmec_surf.signgs/(2π),α₀,ζ_range)
    PlasmaEquilibriumToolkit.writeGeneGeometry(filename,points_pest,vmec_surf,metric,modB,sqrtg,K1,K2,∂B)
end