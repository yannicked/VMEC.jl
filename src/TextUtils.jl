"""
    readVmecWoutText(wout::AbstractString)
    readVmecWoutText(wout::IOStream)

    Read in a Text VMEC wout file and populate the associated Vmec and VmecData structs.  Currently
    only works for wout files with version 8.47 and higher

# See also: [`VmecData`](@ref)
"""
function readVmecWoutText(wout::IOStream)
  vmec_version = parse(Float64, split(readline(wout), '=')[2])
  if vmec_version < 8.47
    error("Only version 8.47 is supported")
  end
  include_js_line = if vmec_version == 9.0
    true
  elseif vmec_version >= 8.47
    false
  else
    error("Only version 8.47 to 9.0 is supported")
  end
  wb, wp, gamma, pfac, rmax_surf, rmin_surf, zmax_surf = parse.(Float64, split(readline(wout), ' ', keepempty=false))
  nfp, ns, mpol, ntor, mnmax, mnmax_nyq, itfsq, niter, iasym, ireconstruct, ierr_vmec = parse.(Int32, split(readline(wout), ' ', keepempty=false))
  imse, itse, nbsets, nobd, nextcur, nstore_seq = parse.(Int32, split(readline(wout), ' ', keepempty=false))
  mgrid_file = split(readline(wout), ' ', keepempty=false)[1]
  lasym = convert(Bool, iasym)
  lfreeb = convert(Bool, iasym)
  vmec = VmecData(ns, mnmax, mnmax_nyq, lasym; FT=Float64, lfreeb=lfreeb)
  for js = 1:ns
    if include_js_line
      js_line = split(readline(wout), ' ', keepempty=false)
    end
    for mn = 1:mnmax
      if js == 1
        vmec.xm[mn], vmec.xn[mn] = parse.(Float64, split(readline(wout), ' ', keepempty=false))
      end
      vmec.rmnc[mn, js], vmec.zmns[mn, js], vmec.lmns[mn, js] = parse.(Float64, split(readline(wout), ' ', keepempty=false))
      if lasym
        vmec.rmns[mn, js], vmec.zmnc[mn, js], vmec.lmnc[mn, js] = parse.(Float64, split(readline(wout), ' ', keepempty=false))
      end
    end
    for mn = 1:mnmax_nyq
      if js == 1
        vmec.xm_nyq[mn], vmec.xn_nyq[mn] = parse.(Float64, split(readline(wout), ' ', keepempty=false))
      end
      vmec.bmnc[mn, js], vmec.gmnc[mn, js], vmec.bsubumnc[mn, js], vmec.bsubvmnc[mn, js], vmec.bsubsmns[mn, js], vmec.bsupumnc[mn, js], vmec.bsupvmnc[mn, js] = parse.(Float64, split(readline(wout), ' ', keepempty=false))
      if lasym
        vmec.bmns[mn, js], vmec.gmns[mn, js], vmec.bsubumns[mn, js], vmec.bsubvmns[mn, js], vmec.bsubsmnc[mn, js], vmec.bsupumns[mn, js], vmec.bsupvmns[mn, js] = parse.(Float64, split(readline(wout), ' ', keepempty=false))
      end
    end
  end

  l = reshape(parse.(Float64, split(readline(wout), ' ', keepempty=false)), (6, ns))
  vmec.iotaf = l[1, :]
  vmec.presf = l[2, :]
  vmec.phipf = l[3, :]
  vmec.phi = l[4, :]
  vmec.jcuru = l[5, :]
  vmec.jcurv = l[6, :]
  vmec.chipf = vmec.iotaf .* vmec.phipf
  for js = 1:ns
    # the poloidal flux is not included in the text ouptut? We can try to calculate it ourselves
    if js > 1
      h = 1 / ns
      vmec.chi[js] = vmec.chi[js-1] + vmec.chipf[js-1] * h + (vmec.chipf[js] - vmec.chipf[js-1]) * h * 1 / 2
    end
  end

  l = reshape(parse.(Float64, split(readline(wout), ' ', keepempty=false)), (10, ns - 1))
  vmec.iotas[2:end] = l[1, :]
  vmec.mass[2:end] = l[2, :]
  vmec.pres[2:end] = l[3, :]
  vmec.beta_vol[2:end] = l[4, :]
  vmec.phips[2:end] = l[5, :]
  vmec.buco[2:end] = l[6, :]
  vmec.bvco[2:end] = l[7, :]
  vmec.vp[2:end] = l[8, :]
  vmec.over_r[2:end] = l[9, :]
  vmec.specw[2:end] = l[10, :]
  # for js = 2:ns
  #   vmec.iotas[js], vmec.mass[js], vmec.pres[js], vmec.beta_vol[js], vmec.phips[js], vmec.buco[js], vmec.bvco[js], vmec.vp[js], vmec.over_r[js], vmec.specw[js] = l[((js-2)*10+1):((js-2)*10+1+9)]
  # end

  vmec.aspect, vmec.betatotal, vmec.betapol, vmec.betator, vmec.betaxis, vmec.b0 = parse.(Float64, split(readline(wout), ' ', keepempty=false))

  vmec.signgs = parse.(Int32, split(readline(wout), ' ', keepempty=false))[1]

  input_ext = split(readline(wout), ' ', keepempty=false)[1]

  vmec.IonLarmor, vmec.volavgB, vmec.rbtor0, vmec.rbtor, ctor, vmec.Aminor_p, vmec.Rmajor_p, vmec.volume_p = parse.(Float64, split(readline(wout), ' ', keepempty=false))

  l = reshape(parse.(Float64, split(readline(wout), ' ', keepempty=false)), (6, ns - 2))
  vmec.DMerc[2:end-1] = l[1, :]
  vmec.DShear[2:end-1] = l[2, :]
  vmec.DWell[2:end-1] = l[3, :]
  vmec.DCurr[2:end-1] = l[4, :]
  vmec.DGeod[2:end-1] = l[5, :]
  vmec.equif[2:end-1] = l[6, :]
  # for js = 2:(ns-1)
  #   vmec.DMerc[js], vmec.DShear[js], vmec.DWell[js], vmec.DCurr[js], vmec.DGeod[js], vmec.equif[js] = l[((js-2)*6+1):((js-2)*6+1+5)]
  # end

  if nextcur > 0
    error("Not implemented")
  end


  l = reshape(parse.(Float64, split(readline(wout), ' ', keepempty=false)), (2, nstore_seq))

  fsqt = l[1, :]
  wdot = l[2, :]

  l = reshape(parse.(Float64, split(readline(wout), ' ', keepempty=false)), (3, ns))
  vmec.jdotb = l[1, :]
  vmec.bdotgradv = l[2, :]
  vmec.bdotb = l[3, :]
  # for js = 1:ns
  #   vmec.jdotb[js], vmec.bdotgradv[js], vmec.bdotb[js] = l[((js-1)*3+1):((js-1)*3+1+2)]
  # end

  vmec.gamma = gamma
  vmec.wb = wb
  vmec.wp = wp
  vmec.rmax_surf = rmax_surf
  vmec.rmin_surf = rmin_surf
  vmec.zmax_surf = zmax_surf
  vmec.nfp = nfp
  vmec.mpol = mpol
  vmec.ntor = ntor

  return Vmec(vmec)
end

function readVmecWoutText(wout::AbstractString)
  wout_stream = open(wout)
  return readVmecWoutText(wout_stream)
end