
# Provide aliases for current type names to avoid breakage 
VmecFourierData = PlasmaEquilibriumToolkit.SurfaceFourierData
VmecFourierArray = PlasmaEquilibriumToolkit.SurfaceFourierArray

"""
    VmecData{FT} where FT <: AbstractFloat

Mutable type containing the output data from a VMEC equilibrium calculation.  There is a one-to-one
correspondence between VmecData property names and Fortran variables.
"""
mutable struct VmecData{FT} <: PlasmaEquilibriumToolkit.AbstractMagneticEquilibrium
  rmnc::Array{FT,2}
  rmns::Array{FT,2}
  zmnc::Array{FT,2}
  zmns::Array{FT,2}
  lmnc::Array{FT,2}
  lmns::Array{FT,2}
  bmnc::Array{FT,2}
  bmns::Array{FT,2}
  gmnc::Array{FT,2}
  gmns::Array{FT,2}
  bsubsmnc::Array{FT,2}
  bsubsmns::Array{FT,2}
  bsubumnc::Array{FT,2}
  bsubumns::Array{FT,2}
  bsubvmnc::Array{FT,2}
  bsubvmns::Array{FT,2}
  bsupumnc::Array{FT,2}
  bsupumns::Array{FT,2}
  bsupvmnc::Array{FT,2}
  bsupvmns::Array{FT,2}
  currumnc::Array{FT,2}
  currumns::Array{FT,2}
  currvmnc::Array{FT,2}
  currvmns::Array{FT,2}

  xm::Array{FT,1}
  xn::Array{FT,1}
  xm_nyq::Array{FT,1}
  xn_nyq::Array{FT,1}
  phi::Array{FT,1}
  phips::Array{FT,1}
  phipf::Array{FT,1}
  chi::Array{FT,1}
  chipf::Array{FT,1}
  iotaf::Array{FT,1}
  iotas::Array{FT,1}
  pres::Array{FT,1}
  presf::Array{FT,1}
  beta_vol::Array{FT,1}
  raxis_cc::Array{FT,1}
  raxis_cs::Array{FT,1}
  zaxis_cs::Array{FT,1}
  zaxis_cc::Array{FT,1}
  am::Array{FT,1}
  am_aux_s::Array{FT,1}
  am_aux_f::Array{FT,1}
  ac::Array{FT,1}
  ac_aux_s::Array{FT,1}
  ac_aux_f::Array{FT,1}
  ai::Array{FT,1}
  ai_aux_s::Array{FT,1}
  ai_aux_f::Array{FT,1}
  jcuru::Array{FT,1}
  jcurv::Array{FT,1}
  mass::Array{FT,1}
  buco::Array{FT,1}
  bvco::Array{FT,1}
  vp::Array{FT,1}
  specw::Array{FT,1}
  over_r::Array{FT,1}
  jdotb::Array{FT,1}
  bdotb::Array{FT,1}
  bdotgradv::Array{FT,1}
  DMerc::Array{FT,1}
  DShear::Array{FT,1}
  DWell::Array{FT,1}
  DCurr::Array{FT,1}
  DGeod::Array{FT,1}
  equif::Array{FT,1}

  Aminor_p::FT
  Rmajor_p::FT
  aspect::FT
  volume_p::FT
  wb::FT
  wp::FT
  gamma::FT
  rmax_surf::FT
  rmin_surf::FT
  zmax_surf::FT
  betatotal::FT
  betator::FT
  betapol::FT
  betaxis::FT
  b0::FT
  rbtor0::FT
  rbtor::FT
  IonLarmor::FT
  volavgB::FT

  mnmax::Int32
  mnmax_nyq::Int32
  ns::Int32
  mpol::Int32
  ntor::Int32
  signgs::Int32
  nfp::Int32

  pcurr_type::String
  piota_type::String
  pmass_type::String

  lasym::Bool
  lfreeb::Bool

  filename_nc::String
  function VmecData(ns_in::I,
                    mnmax_in::I,
                    mnmax_nyq::I,
                    lasym_in::Bool;
                    FT::Type=Float64,
                    lfreeb::Bool=false,
                   ) where {I <: Integer}
    ntord = 101
    ndatafmax = 101
    nexp = 20
    lasym = lasym_in
    lfreeb = lfreeb
    ns = convert(Int32,ns_in)
    mnmax = convert(Int32,mnmax_in)
    mnmax_nyq = convert(Int32,mnmax_nyq)

    rmnc = zeros(FT,mnmax,ns)
    zmns = zeros(FT,mnmax,ns)
    lmns = zeros(FT,mnmax,ns)
    bmnc = zeros(FT,mnmax_nyq,ns)
    gmnc = zeros(FT,mnmax_nyq,ns)
    bsubsmns = zeros(FT,mnmax_nyq,ns)
    bsubumnc = zeros(FT,mnmax_nyq,ns)
    bsubvmnc = zeros(FT,mnmax_nyq,ns)
    bsupumnc = zeros(FT,mnmax_nyq,ns)
    bsupvmnc = zeros(FT,mnmax_nyq,ns)
    currumnc = zeros(FT,mnmax_nyq,ns)
    currvmnc = zeros(FT,mnmax_nyq,ns)

    rmns = zeros(FT,mnmax,ns)
    zmnc = zeros(FT,mnmax,ns)
    lmnc = zeros(FT,mnmax,ns)
    bmns = zeros(FT,mnmax_nyq,ns)
    gmns = zeros(FT,mnmax_nyq,ns)
    bsubsmnc = zeros(FT,mnmax_nyq,ns)
    bsubumns = zeros(FT,mnmax_nyq,ns)
    bsubvmns = zeros(FT,mnmax_nyq,ns)
    bsupumns = zeros(FT,mnmax_nyq,ns)
    bsupvmns = zeros(FT,mnmax_nyq,ns)
    currumns = zeros(FT,mnmax_nyq,ns)
    currvmns = zeros(FT,mnmax_nyq,ns)

    xm = zeros(FT,mnmax)
    xn = zeros(FT,mnmax)
    xm_nyq = zeros(FT,mnmax_nyq)
    xn_nyq = zeros(FT,mnmax_nyq)
    phi = zeros(FT,ns)
    phips = zeros(FT,ns)
    phipf = zeros(FT,ns)
    chi = zeros(FT,ns)
    chipf = zeros(FT,ns)
    iotas = zeros(FT,ns)
    iotaf = zeros(FT,ns)
    pres = zeros(FT,ns)
    presf = zeros(FT,ns)
    beta_vol = zeros(FT,ns)

    jcuru = zeros(FT,ns)
    jcurv = zeros(FT,ns)
    mass = zeros(FT,ns)
    buco = zeros(FT,ns)
    bvco = zeros(FT,ns)
    vp = zeros(FT,ns)
    specw = zeros(FT,ns)
    over_r = zeros(FT,ns)
    jdotb = zeros(FT,ns)
    bdotb = zeros(FT,ns)
    bdotgradv = zeros(FT,ns)
    DMerc = zeros(FT,ns)
    DShear = zeros(FT,ns)
    DWell = zeros(FT,ns)
    DCurr = zeros(FT,ns)
    DGeod = zeros(FT,ns)
    equif = zeros(FT,ns)

    raxis_cc = zeros(FT,ntord)
    raxis_cs = zeros(FT,ntord)
    zaxis_cc = zeros(FT,ntord)
    zaxis_cs = zeros(FT,ntord)
    am = zeros(FT,nexp)
    am_aux_s = zeros(FT,ndatafmax)
    am_aux_f = zeros(FT,ndatafmax)
    ac = zeros(FT,nexp)
    ac_aux_s = zeros(FT,ndatafmax)
    ac_aux_f = zeros(FT,ndatafmax)
    ai = zeros(FT,nexp)
    ai_aux_s = zeros(FT,ndatafmax)
    ai_aux_f = zeros(FT,ndatafmax)

    Aminor_p = zero(FT)
    Rmajor_p = zero(FT)
    aspect = zero(FT)
    volume_p = zero(FT)
    wb = zero(FT)
    wp = zero(FT)
    gamma = zero(FT)
    rmax_surf = zero(FT)
    rmin_surf = zero(FT)
    zmax_surf = zero(FT)
    betatotal = zero(FT)
    betapol = zero(FT)
    betator = zero(FT)
    betaxis = zero(FT)
    b0 = zero(FT)
    rbtor0 = zero(FT)
    rbtor = zero(FT)
    IonLarmor = zero(FT)
    volavgB = zero(FT)

    mpol = zero(I)
    ntor = zero(I)
    signgs = zero(I)
    nfp = zero(I)

    pcurr_type = "power_series"
    pmass_type = "power_series"
    piota_type = "power_series"
    
    return new{FT}(rmnc,rmns,zmnc,zmns,lmnc,lmns,bmnc,bmns,gmnc,gmns,
               bsubsmnc,bsubsmns,bsubumnc,bsubumns,bsubvmnc,bsubvmns,
               bsupumnc,bsupumns,bsupvmnc,bsupvmns,
               currumnc,currumns,currvmnc,currvmns,
               xm,xn,xm_nyq,xn_nyq,phi,phips,phipf,chi,chipf,iotaf,iotas,pres,presf,beta_vol,
               raxis_cc,raxis_cs,zaxis_cs,zaxis_cc,
               am,am_aux_s,am_aux_f,ac,ac_aux_s,ac_aux_f,ai,ai_aux_s,ai_aux_f,
               jcuru,jcurv,mass,buco,bvco,vp,specw,over_r,jdotb,bdotb,bdotgradv,
               DMerc,DShear,DWell,DCurr,DGeod,equif,
               Aminor_p,Rmajor_p,aspect,volume_p,wb,wp,gamma,rmax_surf,rmin_surf,zmax_surf,
               betatotal,betator,betapol,betaxis,b0,rbtor0,rbtor,IonLarmor,volavgB,
               mnmax,mnmax_nyq,ns,mpol,ntor,signgs,nfp,pcurr_type,piota_type,pmass_type,lasym,lfreeb,"")
  end
  #Incomplete constructor to initialize empty structs
  VmecData{FT}() where {FT <: AbstractFloat} = new{FT}()

end


struct VmecFourierSpline{T}
  m::T
  n::T
  cos::Interpolations.Extrapolation
  sin::Interpolations.Extrapolation
end



"""
    Vmec{T} <: AbstractMagneticEquilibrium

Data structure containing the Vmec data in a format more conducive for efficient calculation of physical
quantities through inverse Fourier transforms.  The radial (or ``s``) dependence of quantities is computed
through a cubic B-spline.
"""
struct Vmec{T} <: PlasmaEquilibriumToolkit.AbstractMagneticEquilibrium
  rmn::Vector{VmecFourierSpline{T}}
  zmn::Vector{VmecFourierSpline{T}}
  λmn::Vector{VmecFourierSpline{T}}
  bmn::Vector{VmecFourierSpline{T}}
  gmn::Vector{VmecFourierSpline{T}}
  bsubsmn::Vector{VmecFourierSpline{T}}
  bsubumn::Vector{VmecFourierSpline{T}}
  bsubvmn::Vector{VmecFourierSpline{T}}
  bsupumn::Vector{VmecFourierSpline{T}}
  bsupvmn::Vector{VmecFourierSpline{T}}
  currumn::Vector{VmecFourierSpline{T}}
  currvmn::Vector{VmecFourierSpline{T}}

  xm::Array{T,1}
  xn::Array{T,1}
  xm_nyq::Array{T,1}
  xn_nyq::Array{T,1}
  phi::Interpolations.Extrapolation
  chi::Interpolations.Extrapolation
  pres::Interpolations.Extrapolation
  iota::Interpolations.Extrapolation
  beta_vol::Interpolations.Extrapolation
  raxis_cc::Array{T,1}
  raxis_cs::Array{T,1}
  zaxis_cs::Array{T,1}
  zaxis_cc::Array{T,1}
  am::Array{T,1}
  am_aux_s::Array{T,1}
  am_aux_f::Array{T,1}
  ac::Array{T,1}
  ac_aux_s::Array{T,1}
  ac_aux_f::Array{T,1}
  ai::Array{T,1}
  ai_aux_s::Array{T,1}
  ai_aux_f::Array{T,1}
  jcuru::Interpolations.Extrapolation
  jcurv::Interpolations.Extrapolation
  mass::Interpolations.Extrapolation
  buco::Interpolations.Extrapolation
  bvco::Interpolations.Extrapolation
  vp::Interpolations.Extrapolation
  specw::Interpolations.Extrapolation
  over_r::Interpolations.Extrapolation
  jdotb::Interpolations.Extrapolation
  bdotb::Interpolations.Extrapolation
  bdotgradv::Interpolations.Extrapolation
  DMerc::Interpolations.Extrapolation
  DShear::Interpolations.Extrapolation
  DWell::Interpolations.Extrapolation
  DCurr::Interpolations.Extrapolation
  DGeod::Interpolations.Extrapolation
  equif::Interpolations.Extrapolation

  Aminor_p::T
  Rmajor_p::T
  aspect::T
  volume_p::T
  wb::T
  wp::T
  gamma::T
  rmax_surf::T
  rmin_surf::T
  zmax_surf::T
  betatotal::T
  betator::T
  betapol::T
  betaxis::T
  b0::T
  rbtor0::T
  rbtor::T
  IonLarmor::T
  volavgB::T

  mnmax::Int32
  mnmax_nyq::Int32
  ns::Int32
  mpol::Int32
  ntor::Int32
  signgs::Int32
  nfp::Int32

  pcurr_type::String
  piota_type::String
  pmass_type::String

  lasym::Bool
  lfreeb::Bool

  Vmec{T}() where {T} = new{T}()
  Vmec{T}(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,aa,ab,ac,ad,ae,af,ag,ah,
    ai,aj,ak,al,am,an,ao,ap,aq,ar,as,at,au,av,aw,ax,ay,az,ba,bb,bc,bd,be,bf,bg,bh,bi,
    bj,bk,bl,bm,bn,bo,bp,bq,br,bs,bt,bu,bv,bw,bx,by,bz,ca,cb,cc,cd) where {T} = new{T}(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,aa,ab,ac,ad,ae,af,ag,ah,
    ai,aj,ak,al,am,an,ao,ap,aq,ar,as,at,au,av,aw,ax,ay,az,ba,bb,bc,bd,be,bf,bg,bh,bi,
    bj,bk,bl,bm,bn,bo,bp,bq,br,bs,bt,bu,bv,bw,bx,by,bz,ca,cb,cc,cd)

end

"""
    Vmec(vmec::VmecData)
    Vmec(m_pol::Int, n_tor::Int, ns::Int)

Compute a `Vmec` structure from the raw `VmecData` data.

See also: [`VmecData`](@ref)
"""
function Vmec(vmec::VmecData{T};
             ) where {T}
  ns = vmec.ns
  fullGridKnots = range(0.0,step=1.0/(ns-1),length=ns)
  halfGridKnots = range(0.5/(vmec.ns-1),step=1.0/(ns-1),length=ns-1)
  zeroFullGridInterp = cubic_spline_interpolation(fullGridKnots,zeros(T,ns))
  zeroHalfGridInterp = cubic_spline_interpolation(halfGridKnots,zeros(T,ns-1))
  rmn = Vector{VmecFourierSpline{T}}(undef,vmec.mnmax)
  zmn = similar(rmn)
  λmn = similar(rmn)
  bmn = Vector{VmecFourierSpline{T}}(undef,vmec.mnmax_nyq)
  gmn = similar(bmn)
  bsubsmn = similar(bmn)
  bsubumn = similar(bmn)
  bsubvmn = similar(bmn)
  bsupumn = similar(bmn)
  bsupvmn = similar(bmn)
  currumn = similar(bmn)
  currvmn = similar(bmn)

  @batch for i = 1:vmec.mnmax
    rmn[i] = VmecFourierSpline{T}(vmec.xm[i],vmec.xn[i],cubic_spline_interpolation(fullGridKnots,view(vmec.rmnc,i,:),extrapolation_bc=Line()),cubic_spline_interpolation(fullGridKnots,view(vmec.rmns,i,:),extrapolation_bc=Line()))
    zmn[i] = VmecFourierSpline{T}(vmec.xm[i],vmec.xn[i],cubic_spline_interpolation(fullGridKnots,view(vmec.zmnc,i,:),extrapolation_bc=Line()),cubic_spline_interpolation(fullGridKnots,view(vmec.zmns,i,:),extrapolation_bc=Line()))
    λmn[i] = VmecFourierSpline{T}(vmec.xm[i],vmec.xn[i],cubic_spline_interpolation(fullGridKnots,view(vmec.lmnc,i,:),extrapolation_bc=Line()),cubic_spline_interpolation(fullGridKnots,view(vmec.lmns,i,:),extrapolation_bc=Line()))
  end

  @batch for i = 1:vmec.mnmax_nyq
    bmn[i] = VmecFourierSpline{T}(vmec.xm_nyq[i],vmec.xn_nyq[i],cubic_spline_interpolation(halfGridKnots,view(vmec.bmnc,i,2:ns),extrapolation_bc=Line()),cubic_spline_interpolation(halfGridKnots,view(vmec.bmns,i,2:ns),extrapolation_bc=Line()))
    gmn[i] = VmecFourierSpline{T}(vmec.xm_nyq[i],vmec.xn_nyq[i],cubic_spline_interpolation(halfGridKnots,view(vmec.gmnc,i,2:ns),extrapolation_bc=Line()),cubic_spline_interpolation(halfGridKnots,view(vmec.gmns,i,2:ns),extrapolation_bc=Line()))
    bsubsmn[i] = VmecFourierSpline{T}(vmec.xm_nyq[i],vmec.xn_nyq[i],cubic_spline_interpolation(fullGridKnots,view(vmec.bsubsmnc,i,:),extrapolation_bc=Line()),cubic_spline_interpolation(fullGridKnots,view(vmec.bsubsmns,i,:),extrapolation_bc=Line()))
    bsubumn[i] = VmecFourierSpline{T}(vmec.xm_nyq[i],vmec.xn_nyq[i],cubic_spline_interpolation(halfGridKnots,view(vmec.bsubumnc,i,2:ns),extrapolation_bc=Line()),cubic_spline_interpolation(halfGridKnots,view(vmec.bsubumns,i,2:ns),extrapolation_bc=Line()))
    bsubvmn[i] = VmecFourierSpline{T}(vmec.xm_nyq[i],vmec.xn_nyq[i],cubic_spline_interpolation(halfGridKnots,view(vmec.bsubvmnc,i,2:ns),extrapolation_bc=Line()),cubic_spline_interpolation(halfGridKnots,view(vmec.bsubvmns,i,2:ns),extrapolation_bc=Line()))
    bsupumn[i] = VmecFourierSpline{T}(vmec.xm_nyq[i],vmec.xn_nyq[i],cubic_spline_interpolation(halfGridKnots,view(vmec.bsupumnc,i,2:ns),extrapolation_bc=Line()),cubic_spline_interpolation(halfGridKnots,view(vmec.bsupumns,i,2:ns),extrapolation_bc=Line()))
    bsupvmn[i] = VmecFourierSpline{T}(vmec.xm_nyq[i],vmec.xn_nyq[i],cubic_spline_interpolation(halfGridKnots,view(vmec.bsupvmnc,i,2:ns),extrapolation_bc=Line()),cubic_spline_interpolation(halfGridKnots,view(vmec.bsupvmns,i,2:ns),extrapolation_bc=Line()))
    currumn[i] = VmecFourierSpline{T}(vmec.xm_nyq[i],vmec.xn_nyq[i],cubic_spline_interpolation(fullGridKnots,view(vmec.currumnc,i,:),extrapolation_bc=Line()),cubic_spline_interpolation(fullGridKnots,view(vmec.currumns,i,:),extrapolation_bc=Line()))
    currvmn[i] = VmecFourierSpline{T}(vmec.xm_nyq[i],vmec.xn_nyq[i],cubic_spline_interpolation(fullGridKnots,view(vmec.currvmnc,i,:),extrapolation_bc=Line()),cubic_spline_interpolation(fullGridKnots,view(vmec.currvmns,i,:),extrapolation_bc=Line()))
  end

  phi = isdefined(vmec,:phi) ? cubic_spline_interpolation(fullGridKnots,getfield(vmec,:phi), extrapolation_bc=Line()) : zeroFullGridInterp
  chi = isdefined(vmec,:chi) ? cubic_spline_interpolation(fullGridKnots,getfield(vmec,:chi), extrapolation_bc=Line()) : zeroFullGridInterp
  pres = isdefined(vmec,:presf) ? cubic_spline_interpolation(fullGridKnots,getfield(vmec,:presf), extrapolation_bc=Line()) : zeroFullGridInterp
  iota = isdefined(vmec,:iotaf) ? cubic_spline_interpolation(fullGridKnots,getfield(vmec,:iotaf), extrapolation_bc=Line()) : zeroFullGridInterp
  beta_vol = isdefined(vmec,:beta_vol) ? cubic_spline_interpolation(halfGridKnots,view(getfield(vmec,:beta_vol),2:ns), extrapolation_bc=Line()) : zeroHalfGridInterp
  jcuru = isdefined(vmec,:jcuru) ? cubic_spline_interpolation(fullGridKnots,getfield(vmec,:jcuru), extrapolation_bc=Line()) : zeroFullGridInterp
  jcurv = isdefined(vmec,:jcurv) ? cubic_spline_interpolation(fullGridKnots,getfield(vmec,:jcurv), extrapolation_bc=Line()) : zeroFullGridInterp
  mass = isdefined(vmec,:mass) ? cubic_spline_interpolation(halfGridKnots,view(getfield(vmec,:mass),2:ns), extrapolation_bc=Line()) : zeroHalfGridInterp
  buco = isdefined(vmec,:buco) ? cubic_spline_interpolation(halfGridKnots,view(getfield(vmec,:buco),2:ns), extrapolation_bc=Line()) : zeroHalfGridInterp
  bvco = isdefined(vmec,:bvco) ? cubic_spline_interpolation(halfGridKnots,view(getfield(vmec,:bvco),2:ns), extrapolation_bc=Line()) : zeroHalfGridInterp
  vp = isdefined(vmec,:vp) ? cubic_spline_interpolation(halfGridKnots,view(getfield(vmec,:vp),2:ns), extrapolation_bc=Line()) : zeroHalfGridInterp
  specw = isdefined(vmec,:specw) ? cubic_spline_interpolation(halfGridKnots,view(getfield(vmec,:specw),2:ns), extrapolation_bc=Line()) : zeroHalfGridInterp
  over_r = isdefined(vmec,:over_r) ? cubic_spline_interpolation(fullGridKnots,getfield(vmec,:over_r), extrapolation_bc=Line()) : zeroFullGridInterp
  jdotb = isdefined(vmec,:jdotb) ? cubic_spline_interpolation(fullGridKnots,getfield(vmec,:jdotb), extrapolation_bc=Line()) : zeroFullGridInterp
  bdotb = isdefined(vmec,:bdotb) ? cubic_spline_interpolation(fullGridKnots,getfield(vmec,:bdotb), extrapolation_bc=Line()) : zeroFullGridInterp
  bdotgradv = isdefined(vmec,:bdotgradv) ? cubic_spline_interpolation(fullGridKnots,getfield(vmec,:bdotgradv), extrapolation_bc=Line()) : zeroFullGridInterp
  DMerc = isdefined(vmec,:DMerc) ? cubic_spline_interpolation(fullGridKnots,getfield(vmec,:DMerc), extrapolation_bc=Line()) : zeroFullGridInterp
  DShear = isdefined(vmec,:DShear) ? cubic_spline_interpolation(fullGridKnots,getfield(vmec,:DShear), extrapolation_bc=Line()) : zeroFullGridInterp
  DWell = isdefined(vmec,:DWell) ? cubic_spline_interpolation(fullGridKnots,getfield(vmec,:DWell), extrapolation_bc=Line()) : zeroFullGridInterp
  DCurr = isdefined(vmec,:DCurr) ? cubic_spline_interpolation(fullGridKnots,getfield(vmec,:DCurr), extrapolation_bc=Line()) : zeroFullGridInterp
  DGeod = isdefined(vmec,:DGeod) ? cubic_spline_interpolation(fullGridKnots,getfield(vmec,:DGeod), extrapolation_bc=Line()) : zeroFullGridInterp
  equif = isdefined(vmec,:equif) ? cubic_spline_interpolation(fullGridKnots,getfield(vmec,:equif), extrapolation_bc=Line()) : zeroFullGridInterp

  nfp = getfield(vmec,:nfp)
  mpol = getfield(vmec,:mpol)
  ntor = getfield(vmec,:ntor)
  signgs = getfield(vmec,:signgs)
  mnmax = getfield(vmec,:mnmax)
  mnmax_nyq = getfield(vmec,:mnmax_nyq)
  ns = getfield(vmec,:ns)

  xm = getfield(vmec,:xm)
  xn = getfield(vmec,:xn)
  xm_nyq = getfield(vmec,:xm_nyq)
  xn_nyq = getfield(vmec,:xn_nyq)

  Aminor_p = getfield(vmec,:Aminor_p)
  Rmajor_p = getfield(vmec,:Rmajor_p)
  aspect = getfield(vmec,:aspect)
  volume_p = getfield(vmec,:volume_p)
  wb = getfield(vmec,:wb)
  wp = getfield(vmec,:wp)
  gamma = getfield(vmec,:gamma)
  rmax_surf = getfield(vmec,:rmax_surf)
  rmin_surf = getfield(vmec,:rmin_surf)
  zmax_surf = getfield(vmec,:zmax_surf)
  betatotal = getfield(vmec,:betatotal)
  betator = getfield(vmec,:betator)
  betapol = getfield(vmec,:betapol)
  betaxis = getfield(vmec,:betaxis)
  b0 = getfield(vmec,:b0)
  rbtor0 = getfield(vmec,:rbtor0)
  rbtor = getfield(vmec,:rbtor)
  IonLarmor = getfield(vmec,:IonLarmor)
  volavgB = getfield(vmec,:volavgB)

  pcurr_type = getfield(vmec,:pcurr_type)
  piota_type = getfield(vmec,:piota_type)
  pmass_type = getfield(vmec,:pmass_type)

  lasym = getfield(vmec,:lasym)
  lfreeb = getfield(vmec,:lfreeb)

  raxis_cc = getfield(vmec,:raxis_cc)
  raxis_cs = getfield(vmec,:raxis_cs)
  zaxis_cs = getfield(vmec,:zaxis_cs)
  zaxis_cc = getfield(vmec,:zaxis_cc)

  am = getfield(vmec,:am)
  am_aux_s = getfield(vmec,:am_aux_s)
  am_aux_f = getfield(vmec,:am_aux_f)
  ac = getfield(vmec,:ac)
  ac_aux_s = getfield(vmec,:ac_aux_s)
  ac_aux_f = getfield(vmec,:ac_aux_f)
  ai = getfield(vmec,:ai)
  ai_aux_s = getfield(vmec,:ai_aux_s)
  ai_aux_f = getfield(vmec,:ai_aux_f)

  return Vmec{T}(rmn,zmn,λmn,bmn,gmn,bsubsmn,bsubumn,bsubvmn,bsupumn,bsupvmn,currumn,currvmn,
                 xm,xn,xm_nyq,xn_nyq,phi,chi,pres,iota,beta_vol,
                 raxis_cc,raxis_cs,zaxis_cs,zaxis_cc,
                 am,am_aux_s,am_aux_f,ac,ac_aux_s,ac_aux_f,ai,ai_aux_s,ai_aux_f,
                 jcuru,jcurv,mass,buco,bvco,vp,specw,over_r,jdotb,bdotb,bdotgradv,
                 DMerc,DShear,DWell,DCurr,DGeod,equif,
                 Aminor_p,Rmajor_p,aspect,volume_p,wb,wp,gamma,rmax_surf,rmin_surf,zmax_surf,
                 betatotal,betator,betapol,betaxis,b0,rbtor0,rbtor,IonLarmor,volavgB,
                 mnmax,mnmax_nyq,ns,mpol,ntor,signgs,nfp,pcurr_type,piota_type,pmass_type,lasym,lfreeb)
end

function VmecData(vmec::Vmec{T}) where {T}
  vmec_data = VmecData(vmec.ns, vmec.mnmax, vmec.mnmax_nyq, vmec.lasym;
                       FT=T)
  full_grid_knots = range(0.0, step=1.0/(vmec.ns-1), length=vmec.ns)
  half_grid_knots = range(0.5/(vmec.ns-1), step=1.0/(vmec.ns-1), length=vmec.ns-1)
  cos_data = zeros(T, (vmec.mnmax_nyq, vmec.ns))
  sin_data = zeros(T, (vmec.mnmax_nyq, vmec.ns))
  data_1d = zeros(T, vmec.mnmax_nyq)
  point_data = nothing
  for field in fieldnames(VmecData)
    field_string = string(field)
    if occursin(r"[a-z*]mn[c,s]", field_string)
      field_base = split(field_string, "mn")[1]
      cos_sym = Symbol(field_base*"mnc")
      sin_sym = Symbol(field_base*"mns")
      field_base_sym = field_base != "l" ? Symbol(field_base*"mn") : :λmn
      itp_field = getfield(vmec, field_base_sym)
      mnmodes = size(itp_field, 1)
      knots = itp_field[1].cos.itp.ranges[1]
      offset = knots == full_grid_knots ? 0 : 1
      for i in 1:mnmodes
        cos_data[i, 1+offset:vmec.ns] = [itp_field[i].cos(s) for s in knots]
        sin_data[i, 1+offset:vmec.ns] = [itp_field[i].sin(s) for s in knots]
      end
      setfield!(vmec_data, cos_sym, copy(cos_data[1:mnmodes, :]))
      setfield!(vmec_data, sin_sym, copy(sin_data[1:mnmodes, :]))
      fill!(cos_data, zero(T))
      fill!(sin_data, zero(T))
    else
      derivative_match = match(r"([a-zA-Z]*)(p[s,f])", field_string)
      if !isnothing(derivative_match)
        field_base_sym = Symbol(derivative_match.captures[1])
        data_field = getfield(vmec, field_base_sym)
        knots = derivative_match.captures[2] == "pf" ? full_grid_knots : half_grid_knots
        offset = derivative_match.captures[2] == "pf" ? 0 : 1
        data_1d[1+offset:vmec.ns] = [Interpolations.gradient(data_field, s)[] for s in knots]
        setfield!(vmec_data, field, copy(data_1d[1:vmec.ns]))
        fill!(data_1d, zero(T))
      else
        sf_match = match(r"([a-zA-Z]*)([s,f])", field_string)
        if !isnothing(sf_match)
          field_base_sym = Symbol(sf_match.captures[1])
          if hasfield(Vmec, field_base_sym)
            data_field = getfield(vmec, field_base_sym)
            knots = sf_match.captures[2] == "f" ? full_grid_knots : half_grid_knots
            offset = sf_match.captures[2] == "f" ? 0 : 1
            data_1d[1+offset:vmec.ns] = [data_field(s) for s in knots]
            setfield!(vmec_data, field, copy(data_1d[1:vmec.ns]))
            fill!(data_1d, zero(T))
          else
            if hasfield(Vmec, field)
              data_field = getfield(vmec, field)
              if typeof(data_field) <: Interpolations.Extrapolation
                data_1d[1:vmec.ns] = data_field.itp.itp.coefs[1:vmec.ns]
                setfield!(vmec_data, field, copy(data_1d[1:vmec.ns]))
                fill!(data_1d, zero(T))
              elseif typeof(data_field) <: Array
                field_length = length(data_field)
                data_1d[1:field_length] = getfield(vmec, field)[1:field_length]
                setfield!(vmec_data, field, copy(data_1d[1:field_length]))
                fill!(data_1d, zero(T))
              else
                point_data = getfield(vmec, field)
                setfield!(vmec_data, field, isbits(point_data) ? copy(point_data) : point_data)
              end
            end
          end
        else
          if hasfield(Vmec, field)
            data_field = getfield(vmec, field)
            if typeof(data_field) <: Interpolations.Extrapolation
              data_1d[1:vmec.ns] = data_field.itp.itp.coefs[1:vmec.ns]
              setfield!(vmec_data, field, copy(data_1d[1:vmec.ns]))
              fill!(data_1d, zero(T))
            elseif typeof(data_field) <: Array
              field_length = length(data_field)
              data_1d[1:field_length] = getfield(vmec, field)[1:field_length]
              setfield!(vmec_data, field, copy(data_1d[1:field_length]))
              fill!(data_1d, zero(T))
            else
              point_data = getfield(vmec, field)
              setfield!(vmec_data, field, isbits(point_data) ? copy(point_data) : point_data)
            end
          end
        end
      end
    end
  end
  return vmec_data
end

"""
    VmecSurface{T} <: AbstractMagneticEquilibrium

Data structure containing the VMEC data at a *single surface*
in a format more conducive for efficient calculation of physical
quantities through inverse Fourier transforms.  Fields with radial
dependence contain both the value at the surface *and* the derivative
w.r.t to `s`.
"""
mutable struct VmecSurface{T} <: PlasmaEquilibriumToolkit.AbstractMagneticSurface
  rmn::SurfaceFourierArray{T}
  zmn::SurfaceFourierArray{T}
  λmn::SurfaceFourierArray{T}
  bmn::SurfaceFourierArray{T}
  gmn::SurfaceFourierArray{T}
  bsubsmn::SurfaceFourierArray{T}
  bsubumn::SurfaceFourierArray{T}
  bsubvmn::SurfaceFourierArray{T}
  bsupumn::SurfaceFourierArray{T}
  bsupvmn::SurfaceFourierArray{T}
  currumn::SurfaceFourierArray{T}
  currvmn::SurfaceFourierArray{T}

  r::Union{Nothing, Interpolations.Extrapolation}
  z::Union{Nothing, Interpolations.Extrapolation}
  λ::Union{Nothing, Interpolations.Extrapolation}
  b::Union{Nothing, Interpolations.Extrapolation}
  g::Union{Nothing, Interpolations.Extrapolation}
  bsubs::Union{Nothing, Interpolations.Extrapolation}
  bsubu::Union{Nothing, Interpolations.Extrapolation}
  bsubv::Union{Nothing, Interpolations.Extrapolation}
  bsupu::Union{Nothing, Interpolations.Extrapolation}
  bsupv::Union{Nothing, Interpolations.Extrapolation}
  curru::Union{Nothing, Interpolations.Extrapolation}
  currv::Union{Nothing, Interpolations.Extrapolation}

  drds::Union{Nothing, Interpolations.Extrapolation}
  dzds::Union{Nothing, Interpolations.Extrapolation}
  dλds::Union{Nothing, Interpolations.Extrapolation}
  dbds::Union{Nothing, Interpolations.Extrapolation}
  dgds::Union{Nothing, Interpolations.Extrapolation}
  dbsubsds::Union{Nothing, Interpolations.Extrapolation}
  dbsubuds::Union{Nothing, Interpolations.Extrapolation}
  dbsubvds::Union{Nothing, Interpolations.Extrapolation}
  dbsupuds::Union{Nothing, Interpolations.Extrapolation}
  dbsupvds::Union{Nothing, Interpolations.Extrapolation}
  dcurruds::Union{Nothing, Interpolations.Extrapolation}
  dcurrvds::Union{Nothing, Interpolations.Extrapolation}
  
  s::T
  phi::SVector{3,T}
  chi::SVector{3,T}
  pres::SVector{2,T}
  iota::SVector{2,T}
  beta_vol::SVector{2,T}
  jcuru::SVector{2,T}
  jcurv::SVector{2,T}
  mass::SVector{2,T}
  buco::SVector{2,T}
  bvco::SVector{2,T}
  vp::SVector{2,T}
  specw::SVector{2,T}
  over_r::SVector{2,T}
  jdotb::SVector{2,T}
  bdotb::SVector{2,T}
  bdotgradv::SVector{2,T}
  DMerc::SVector{2,T}
  DShear::SVector{2,T}
  DWell::SVector{2,T}
  DCurr::SVector{2,T}
  DGeod::SVector{2,T}
  equif::SVector{2,T}

  nfp::Int
  mpol::Int
  ntor::Int
  signgs::Int
  mnmax::Int
  mnmax_nyq::Int

  xm::Vector{T}
  xn::Vector{T}
  xm_nyq::Vector{T}
  xn_nyq::Vector{T}

  Aminor_p::T
  Rmajor_p::T
  aspect::T

  lasym::Bool
end

"""
    VmecFourierDataArray(s::T,vmec::Vmec{T},field::Symbol)

Construct a `StructArray` with type `SurfaceFourierData` for the VMEC
quantity designated by `field` at the surface designated by `s`.
"""
function VmecFourierDataArray(s::T,
                              vmec::Vmec{T},
                              field::Symbol;
                             ) where {T <: AbstractFloat}
  data = getfield(vmec,field)
  n_modes = length(data)
  m_vec = Vector{T}(undef,n_modes)
  n_vec = similar(m_vec)
  cos_vec = similar(m_vec)
  sin_vec = similar(m_vec)
  dcos_vec = similar(m_vec)
  dsin_vec = similar(m_vec)

  for ind in eachindex(data)
    m_vec[ind] = data[ind].m
    n_vec[ind] = data[ind].n
    cos_vec[ind] = data[ind].cos(s)
    sin_vec[ind] = data[ind].sin(s)
    dcos_vec[ind] = Interpolations.gradient(data[ind].cos,s)[1]
    dsin_vec[ind] = Interpolations.gradient(data[ind].sin,s)[1]
  end

  return StructArray{SurfaceFourierData{T}}((m_vec,n_vec,cos_vec,sin_vec,
                                          dcos_vec,dsin_vec))
end

function VmecSurface(s::T,
                     vmec::Vmec{T};
                    ) where {T <: AbstractFloat}
  mnmax = getfield(vmec,:mnmax)
  mnmax_nyq = getfield(vmec,:mnmax_nyq)

  rmn = VmecFourierDataArray(s,vmec,:rmn)
  zmn = VmecFourierDataArray(s,vmec,:zmn)
  λmn = VmecFourierDataArray(s,vmec,:λmn)
  bmn = VmecFourierDataArray(s,vmec,:bmn)
  gmn = VmecFourierDataArray(s,vmec,:gmn)
  bsubsmn = VmecFourierDataArray(s,vmec,:bsubsmn)
  bsubumn = VmecFourierDataArray(s,vmec,:bsubumn)
  bsubvmn = VmecFourierDataArray(s,vmec,:bsubvmn)
  bsupumn = VmecFourierDataArray(s,vmec,:bsupumn)
  bsupvmn = VmecFourierDataArray(s,vmec,:bsupvmn)
  currumn = VmecFourierDataArray(s,vmec,:currumn)
  currvmn = VmecFourierDataArray(s,vmec,:currvmn)

  phi = SVector{3,T}(vmec.phi(s),Interpolations.gradient(vmec.phi,s)[1],vmec.phi(1.0))
  chi = SVector{3,T}(vmec.chi(s),Interpolations.gradient(vmec.chi,s)[1],vmec.chi(1.0))
  pres = SVector{2,T}(vmec.pres(s),Interpolations.gradient(vmec.pres,s)[1])
  iota = SVector{2,T}(vmec.iota(s),Interpolations.gradient(vmec.iota,s)[1])
  beta_vol = SVector{2,T}(vmec.beta_vol(s),Interpolations.gradient(vmec.beta_vol,s)[1])
  jcuru = SVector{2,T}(vmec.jcuru(s),Interpolations.gradient(vmec.jcuru,s)[1])
  jcurv = SVector{2,T}(vmec.jcurv(s),Interpolations.gradient(vmec.jcurv,s)[1])
  mass = SVector{2,T}(vmec.mass(s),Interpolations.gradient(vmec.mass,s)[1])
  buco = SVector{2,T}(vmec.buco(s),Interpolations.gradient(vmec.buco,s)[1])
  bvco = SVector{2,T}(vmec.bvco(s),Interpolations.gradient(vmec.bvco,s)[1])
  vp = SVector{2,T}(vmec.vp(s),Interpolations.gradient(vmec.vp,s)[1])
  specw = SVector{2,T}(vmec.specw(s),Interpolations.gradient(vmec.specw,s)[1])
  over_r = SVector{2,T}(vmec.over_r(s),Interpolations.gradient(vmec.over_r,s)[1])
  jdotb = SVector{2,T}(vmec.jdotb(s),Interpolations.gradient(vmec.jdotb,s)[1])
  bdotb = SVector{2,T}(vmec.bdotb(s),Interpolations.gradient(vmec.bdotb,s)[1])
  bdotgradv = SVector{2,T}(vmec.bdotgradv(s),Interpolations.gradient(vmec.bdotgradv,s)[1])
  DMerc = SVector{2,T}(vmec.DMerc(s),Interpolations.gradient(vmec.DMerc,s)[1])
  DShear = SVector{2,T}(vmec.DShear(s),Interpolations.gradient(vmec.DShear,s)[1])
  DWell = SVector{2,T}(vmec.DWell(s),Interpolations.gradient(vmec.DWell,s)[1])
  DCurr = SVector{2,T}(vmec.DCurr(s),Interpolations.gradient(vmec.DCurr,s)[1])
  DGeod = SVector{2,T}(vmec.DGeod(s),Interpolations.gradient(vmec.DGeod,s)[1])
  equif = SVector{2,T}(vmec.equif(s),Interpolations.gradient(vmec.equif,s)[1])

  nfp = getfield(vmec,:nfp)
  mpol = getfield(vmec,:mpol)
  ntor = getfield(vmec,:ntor)
  signgs = getfield(vmec,:signgs)

  xm = getfield(vmec,:xm)
  xn = getfield(vmec,:xn)
  xm_nyq = getfield(vmec,:xm_nyq)
  xn_nyq = getfield(vmec,:xn_nyq)

  Aminor_p = getfield(vmec,:Aminor_p)
  Rmajor_p = getfield(vmec,:Rmajor_p)
  aspect = getfield(vmec,:aspect)

  lasym = getfield(vmec,:lasym)

  return VmecSurface{T}(rmn,zmn,λmn,bmn,gmn,
                          bsubsmn,bsubumn,bsubvmn,bsupumn,bsupvmn,currumn,currvmn,
                          nothing,nothing,nothing,nothing,nothing,
                          nothing,nothing,nothing,nothing,nothing,nothing,nothing,
                          nothing,nothing,nothing,nothing,nothing,
                          nothing,nothing,nothing,nothing,nothing,nothing,nothing,
                          s,phi,chi,pres,iota,beta_vol,
                          jcuru,jcurv,mass,buco,bvco,vp,specw,over_r,jdotb,bdotb,bdotgradv,
                          DMerc,DShear,DWell,DCurr,DGeod,equif,
                          nfp,mpol,ntor,signgs,mnmax,mnmax_nyq,xm,xn,xm_nyq,xn_nyq,
                          Aminor_p,Rmajor_p,aspect,lasym)
end

"""
    VmecCoordinates{T,A}(s::T,θ::A,ζ::A)

Coordinates on a magnetic flux surface, where `s` is the surface label and
`θ` and `ζ` are angle-like variables
"""
struct VmecCoordinates{T <: Real,A <: Real} <: PlasmaEquilibriumToolkit.AbstractMagneticCoordinates
  s::T
  θ::A
  ζ::A
  VmecCoordinates{T,A}(s::T,θ::A,ζ::A) where {T,A} = new(s,θ,ζ)
end

function VmecCoordinates(s,θ,ζ)
  s2, θ2, ζ2 = promote(s,θ,ζ)
  return VmecCoordinates{typeof(s2),typeof(θ2)}(s2,θ2,ζ2)
end

Base.show(io::IO, x::VmecCoordinates) = print(io, "VmecCoordinates(s=$(x.s), θ=$(x.θ), ζ=$(x.ζ))")
Base.isapprox(x1::VmecCoordinates, x2::VmecCoordinates; kwargs...) = isapprox(x1.s,x2.s;kwargs...) && isapprox(x1.θ,x2.θ;kwargs...) && isapprox(x1.ζ,x2.ζ;kwargs...)


"""
    VmecCoordinates{T,A}(s::T,θ::A,ζ::A)

Coordinates on a magnetic flux surface, where `s` is the poloidal flux surface label and
`θ` and `ζ` are angle-like variables
"""
struct VmecPoloidalCoordinates{T <: Real,A <: Real} <: PlasmaEquilibriumToolkit.AbstractMagneticCoordinates
  s::T
  θ::A
  ζ::A
  VmecPoloidalCoordinates{T,A}(s::T,θ::A,ζ::A) where {T,A} = new(s,θ,ζ)
end

function VmecPoloidalCoordinates(s,θ,ζ)
  s2, θ2, ζ2 = promote(s,θ,ζ)
  return VmecPoloidalCoordinates{typeof(s2),typeof(θ2)}(s2,θ2,ζ2)
end

# Poloidal version of the PEST coordinate system
# https://xkcd.com/927/
struct PespCoordinates{T<:Real,A<:Real} <: AbstractMagneticCoordinates
  ψ::T
  α::A
  ζ::A
  PespCoordinates{T,A}(ψ::T, α::A, ζ::A) where {T,A} = new(ψ, α, ζ)
end

function PespCoordinates(ψ, α, ζ)
  ψ2, α2, ζ2 = promote(ψ, α, ζ)
  return PespCoordinates{typeof(ψ2),typeof(α2)}(ψ2, α2, ζ2)
end
"""
    BoozerSurface{T}

Data structure containing surface quantities in Boozer coordinates

"""

mutable struct BoozerSurface{T} <: PlasmaEquilibriumToolkit.AbstractMagneticSurface
  mBoozer::Int #redundant, same info is stored in mpol and ntor. Kept as an alias
  nBoozer::Int
  rmn::SurfaceFourierArray{T}
  zmn::SurfaceFourierArray{T}
  λmn::SurfaceFourierArray{T}
  bmn::SurfaceFourierArray{T}
  gmn::SurfaceFourierArray{T}
  νmn::SurfaceFourierArray{T}

  r::Union{Nothing, Interpolations.Extrapolation}
  z::Union{Nothing, Interpolations.Extrapolation}
  λ::Union{Nothing, Interpolations.Extrapolation}
  b::Union{Nothing, Interpolations.Extrapolation}
  g::Union{Nothing, Interpolations.Extrapolation}
  ν::Union{Nothing, Interpolations.Extrapolation}
  #possibly include lesser used categories here?  to discuss
  #could also consider adding interpolations
  s::T
  phi::SVector{3,T}
  chi::SVector{2,T}
  pres::SVector{2,T}
  iota::SVector{2,T}
  beta_vol::SVector{2,T}
  jcuru::SVector{2,T}
  jcurv::SVector{2,T}
  mass::SVector{2,T}
  buco::SVector{2,T}
  bvco::SVector{2,T}
  vp::SVector{2,T}
  specw::SVector{2,T}
  over_r::SVector{2,T}
  jdotb::SVector{2,T}
  bdotb::SVector{2,T}
  bdotgradv::SVector{2,T}
  DMerc::SVector{2,T}
  DShear::SVector{2,T}
  DWell::SVector{2,T}
  DCurr::SVector{2,T}
  DGeod::SVector{2,T}
  equif::SVector{2,T}

  nfp::Int
  mpol::Int
  ntor::Int
  signgs::Int
  mnmax::Int
  mnmax_nyq::Int

  xm::Vector{T}
  xn::Vector{T}
  #xm_nyq is redundant, all boozer quantities have the same sampling
  #but may be useful to carry it around
  xm_nyq::Vector{T}
  xn_nyq::Vector{T}

  Aminor_p::T
  Rmajor_p::T
  aspect::T

  lasym::Bool
end

function BoozerSurface(vmecsurf::VmecSurface{T}, mBoozer::Int, nBoozer::Int) where {T}


  modes, mnBoozer = VMEC.boozer_mode_indices(mBoozer, nBoozer, vmecsurf.nfp)
  scale_factor = 2.0/(2 * (2 * mBoozer + 1) * 2 * (2 * nBoozer + 1))
  #Set up the coordinates, field data and Jacobian of the transformation
  u, b, jac = VMEC.boozer_grid(vmecsurf, mBoozer, nBoozer)
  rmn = VMEC.calculate_boozer_spectrum(:rmn, vmecsurf, mnBoozer, modes,
                                       scale_factor, u, b, jac)
  zmn = VMEC.calculate_boozer_spectrum(:zmn, vmecsurf, mnBoozer, modes,
                                       scale_factor, u, b, jac)
  λmn = VMEC.calculate_boozer_spectrum(:λmn, vmecsurf, mnBoozer, modes,
                                       scale_factor, u, b, jac)
  bmn = VMEC.calculate_boozer_spectrum(:bmn, vmecsurf, mnBoozer, modes,
                                       scale_factor, u, b, jac)
  gmn = VMEC.calculate_boozer_spectrum(:gmn, vmecsurf, mnBoozer, modes,
                                       scale_factor, u, b, jac)
  νmn = VMEC.calculate_boozer_spectrum(:νmn, vmecsurf, mnBoozer, modes,
                                       scale_factor, u, b, jac)

  ψ_edge = vmecsurf.phi[3]
  #we remake all the radial deriv quantities to be d/dψ instead of d/ds
  phi = SVector(vmecsurf.phi[1], vmecsurf.phi[2]/ψ_edge, ψ_edge)
  chi = SVector(vmecsurf.chi[1], vmecsurf.chi[2]/ψ_edge)
  pres = SVector(vmecsurf.pres[1], vmecsurf.pres[2]/ψ_edge)
  iota = SVector(vmecsurf.iota[1], vmecsurf.iota[2]/ψ_edge)
  beta_vol = SVector(vmecsurf.beta_vol[1], vmecsurf.beta_vol[2]/ψ_edge)
  jcuru = SVector(vmecsurf.jcuru[1], vmecsurf.jcuru[2]/ψ_edge)
  jcurv = SVector(vmecsurf.jcurv[1], vmecsurf.jcurv[2]/ψ_edge)
  mass = SVector(vmecsurf.mass[1], vmecsurf.mass[2]/ψ_edge)
  buco = SVector(vmecsurf.buco[1], vmecsurf.buco[2]/ψ_edge)
  bvco = SVector(vmecsurf.bvco[1], vmecsurf.bvco[2]/ψ_edge)
  vp = SVector(vmecsurf.vp[1], vmecsurf.vp[2]/ψ_edge)
  specw = SVector(vmecsurf.specw[1], vmecsurf.specw[2]/ψ_edge)
  over_r = SVector(vmecsurf.over_r[1], vmecsurf.over_r[2]/ψ_edge)
  jdotb = SVector(vmecsurf.jdotb[1], vmecsurf.jdotb[2]/ψ_edge)
  bdotb = SVector(vmecsurf.bdotb[1], vmecsurf.bdotb[2]/ψ_edge)
  bdotgradv = SVector(vmecsurf.bdotgradv[1], vmecsurf.bdotgradv[2]/ψ_edge)
  DMerc = SVector(vmecsurf.DMerc[1], vmecsurf.DMerc[2]/ψ_edge)
  DShear = SVector(vmecsurf.DShear[1], vmecsurf.DShear[2]/ψ_edge)
  DWell = SVector(vmecsurf.DWell[1], vmecsurf.DWell[2]/ψ_edge)
  DCurr = SVector(vmecsurf.DCurr[1], vmecsurf.DCurr[2]/ψ_edge)
  DGeod = SVector(vmecsurf.DGeod[1], vmecsurf.DGeod[2]/ψ_edge)
  equif = SVector(vmecsurf.equif[1], vmecsurf.equif[2]/ψ_edge)

  xm = [mode[1] for mode in modes]
  xn = [mode[2] for mode in modes]

  return BoozerSurface{T}(mBoozer, nBoozer, rmn, zmn, λmn, bmn, gmn, νmn,
                          nothing, nothing, nothing, nothing, nothing, nothing,
                          vmecsurf.s, phi, chi, pres, iota, beta_vol, jcuru,
                          jcurv, mass, buco, bvco, vp, specw, over_r, jdotb,
                          bdotb, bdotgradv, DMerc, DShear, DWell, DCurr, DGeod,
                          equif,
                          vmecsurf.nfp, mBoozer, nBoozer, vmecsurf.signgs,
                          mnBoozer, mnBoozer, xm, xn, xm, xn, vmecsurf.Aminor_p,
                          vmecsurf.Rmajor_p, vmecsurf.aspect, vmecsurf.lasym
                          )
end
