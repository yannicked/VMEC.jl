
struct CylindricalFromVmec <: Transformation; end
struct CartesianFromVmec <: Transformation; end
struct CartesianFromPoloidalVmec <: Transformation; end
struct VmecFromFlux <: Transformation; end
struct FluxFromVmec <: Transformation; end
struct VmecFromPest <: Transformation; end
struct PestFromVmec <: Transformation; end
struct PespFromVmec <: Transformation; end
struct BoozerFromVmec <: Transformation; end
struct BoozerFromFlux <: Transformation; end
struct VmecFromBoozer <: Transformation; end
struct CylindricalFromBoozer <: Transformation; end
struct VmecFromCylindrical <: Transformation; end 
struct VmecFromCartesian <: Transformation; end
struct VmecFromPoloidalVmec <: Transformation; end
struct PoloidalVmecFromVmec <: Transformation; end
struct GeneFromPesp <: Transformation; end
struct CartesianFromPesp <: Transformation; end
struct VmecFromPesp <: Transformation; end
struct FluxFromPesp <: Transformation; end
struct PestFromPesp <: Transformation; end
#struct Derivatives end

function (::VmecFromPoloidalVmec)(x::VmecPoloidalCoordinates, vmecsurf::VmecSurface)
  return VmecCoordinates(vmecsurf.phi[1]/vmecsurf.phi[3], x.θ, x.ζ)
end

function (::PoloidalVmecFromVmec)(x::VmecCoordinates, vmec::Vmec)
  function f(a)
    norm_phi = vmec.phi(a) / vmec.phi(1.0)
    return abs(norm_phi - x.s)
  end

  res = optimize(f, 0.0, 1.0)
  s = res.minimizer[1]

  return VmecPoloidalCoordinates(vmec.chi(s)./vmec.chi(1), x.θ, x.ζ)
end

function (::VmecFromFlux)(x::FluxCoordinates,
                          vmecsurf::VmecSurface;
                         )
  y = FluxCoordinates(x.ψ,x.θ,-x.ζ)
  s = x.ψ*2π/vmecsurf.phi[end]*vmecsurf.signgs
  θ = thetaVmec(y,vmecsurf)
  ζ = -x.ζ
  return VmecCoordinates(s,θ,ζ)
end

function (::CylindricalFromVmec)(x::VmecCoordinates,
                                 vmecsurf::VmecSurface;
                                )
  R = surface_get(x,vmecsurf,:r)
  Z = surface_get(x,vmecsurf,:z)
  ϕ = x.ζ
  return Cylindrical(R,ϕ,Z)
end

function (::CylindricalFromVmec)(x::VmecCoordinates,
                                vmec::Vmec;
                               )
  s = x.s
  rmn = VMEC.VmecFourierDataArray(s, vmec, :rmn)
  zmn = VMEC.VmecFourierDataArray(s, vmec, :zmn)
  r = inverseTransform(x, rmn)
  z = inverseTransform(x, zmn)
  return Cylindrical(r,x.ζ,z)
end


function (::CylindricalFromFlux)(x::FluxCoordinates,
                                 vmecsurf::VmecSurface;
                                )
  v = VmecFromFlux()(x,vmecsurf)
  return CylindricalFromVmec()(v,vmecsurf)
end

#=
function (::CylindricalFromVmec)(x::VmecCoordinates,
                                 vmecsurf::VmecSurface,
                                 ::Derivatives;
                                )
  T = typeof(x.θ)
  d = derivatives(x, vmecsurf)
  return CylindricalFromVmec()(x,vmecsurf), @SMatrix [d[1,1] d[1,2] d[1,3];
                                                  zero(T) zero(T) one(T);
                                                  d[3,1] d[3,2] d[3,3]]
end
=#

function (::CartesianFromVmec)(x::VmecCoordinates,
                               vmecsurf::VmecSurface;
                              )
  c = CylindricalFromVmec()(x,vmecsurf)
  return CartesianFromCylindrical()(c)
end

function (::CartesianFromPoloidalVmec)(x::VmecCoordinates,
                                       vmecsurf::VmecSurface;
                                      )
  return CartesianFromVmec(x,vmecsurf)
end


function (::CartesianFromVmec)(x::VmecCoordinates,
                               vmec::Vmec;
                              )
  cc = CylindricalFromVmec()(x, vmec)
  return CartesianFromCylindrical()(c)
end
  

function (::CartesianFromFlux)(x::FluxCoordinates,
                               vmecsurf::VmecSurface;
                              )
  v = VmecFromFlux(x,vmecsurf)
  return CartesianFromVmec(v,vmecsurf)
end

function (::FluxFromPest)(x::PestCoordinates,
                          vmecsurf::VmecSurface;
                         )
  # VMEC is a left handed coordinate system and a positive rotational transform
  # implies the theta coordinate increases with positive toroidal angle (CCW direction).
  # FluxCoordinates and PestCoordinates are right handed coordinate systems, so to match
  # that ∇ψ > 0 and ∇θ > 0 at (θ,ζ) = (0,0), increasing toroidal angle is negative of the
  # cylindrical toroidal angle and increases CW.  This means the rotational transform
  # in these coordinates is the negative of the rotational transform in VMEC
  return FluxCoordinates(x.ψ,x.α-vmecsurf.iota[1]*x.ζ,x.ζ)
end

function (::FluxFromPesp)(x::PespCoordinates,
                          vmecsurf::VmecSurface;
                         )
  # VMEC is a left handed coordinate system and a positive rotational transform
  # implies the theta coordinate increases with positive toroidal angle (CCW direction).
  # FluxCoordinates and PestCoordinates are right handed coordinate systems, so to match
  # that ∇ψ > 0 and ∇θ > 0 at (θ,ζ) = (0,0), increasing toroidal angle is negative of the
  # cylindrical toroidal angle and increases CW.  This means the rotational transform
  # in these coordinates is the negative of the rotational transform in VMEC
  y = PestFromPesp()(x, vmecsurf)
  return FluxFromPest()(y,vmecsurf)
end

function (::VmecFromPest)(x::PestCoordinates,
                          vmecsurf::VmecSurface;
                         )
  y = FluxFromPest()(x,vmecsurf)
  return VmecFromFlux()(y,vmecsurf)
end

function (::FluxFromVmec)(x::VmecCoordinates,
                        vmecsurf::VmecSurface;
                       )
  ψ = vmecsurf.phi[1]/(2π)*vmecsurf.signgs
  θ = x.θ + inverseTransform(x, vmecsurf.λmn)
  ζ = -x.ζ
  return FluxCoordinates(ψ, θ, ζ)
end

function (::VmecFromPesp)(x::PespCoordinates,
          vmecsurf::VmecSurface;
         )
  y = FluxFromPesp()(x,vmecsurf)
  return VmecFromFlux()(y,vmecsurf)
end


function (::PestFromVmec)(x::VmecCoordinates,
                          vmecsurf::VmecSurface;
                         )
  y = FluxFromVmec()(x, vmecsurf)
  α = y.θ + vmecsurf.iota[1] * y.ζ
  return PestCoordinates(y.ψ, α, y.ζ)
end

function (::PestFromPesp)(x::PespCoordinates,
                          vmecsurf::VmecSurface;
                         )
  ψ = vmecsurf.phi[1]/(2π)*vmecsurf.signgs
  return PestCoordinates(ψ, x.α, x.ζ)
end

function (::PespFromVmec)(x::VmecCoordinates,
                          vmecsurf::VmecSurface;
                         )
  ψ = vmecsurf.chi[1]/(2π)*vmecsurf.signgs
  ζ = - x.ζ
  α = x.θ + inverseTransform(x, vmecsurf.λmn) + vmecsurf.iota[1] * ζ
  return PespCoordinates(ψ, α, ζ)
end

function (::GeneFromPesp)(p::PespCoordinates,
                          vmecsurf::VmecSurface;
                         )
  Φ = vmecsurf.phi[end]*vmecsurf.signgs
  x = sqrt(vmecsurf.phi[1]/vmecsurf.phi[3])
  y = x*p.α
  z = p.α - vmecsurf.iota[1] * p.ζ
  return GeneCoordinates(x, y, z)
end

function (::GeneFromPest)(p::PestCoordinates,
                          vmecsurf::VmecSurface;
                         )
  Φ = vmecsurf.phi[end]*vmecsurf.signgs
  x = sqrt(p.ψ*2π/Φ)
  y = x*p.α
  z = p.α - vmecsurf.iota[1] * p.ζ
  return GeneCoordinates(x, y, z)
end

function (::VmecFromCylindrical)(cc::Cylindrical,
                                 vmec::Vmec)
  #calculate guess for theta
  sg, θg = guess_s_and_θ(cc, vmec)
  #construct bounds arrays
  lower = [0.0, θg - π/2]
  upper = [1.0, θg + π/2]
  guess = [sg, θg]
  ζ = cc.θ

  #optimized function
  function f(x)
    s = x[1]
    θ = x[2]
    vc = VmecCoordinates(s, θ, ζ)
    ccg = CylindricalFromVmec()(vc, vmec)
    return (ccg.r - cc.r)^2 + (ccg.z - cc.z)^2
  end

  res = optimize(f, lower, upper, guess)
  s = res.minimizer[1]
  θ = mod2pi(res.minimizer[2])
  return VmecCoordinates(s, θ, ζ)
end

function (::VmecFromCartesian)(xyz::SVector{3, T}, 
                               vmec::Vmec{T}
                              ) where {T}
  cc = CylindricalFromCartesian()(xyz)
  return VmecFromCylindrical()(cc, vmec)
end

function guess_s_and_θ(x::Cylindrical, vmec::Vmec)
  ζ = x.θ
  rmn_boundary = VMEC.VmecFourierDataArray(1.0, vmec, :rmn)
  zmn_boundary = VMEC.VmecFourierDataArray(1.0, vmec, :zmn)
  rmn_core = VMEC.VmecFourierDataArray(0.0, vmec, :rmn)
  zmn_core = VMEC.VmecFourierDataArray(0.0, vmec, :zmn)
  vc = VmecCoordinates(0.5, 0.0, ζ)
  r_boundary = inverseTransform(vc, rmn_boundary)
  z_boundary = inverseTransform(vc, zmn_boundary)
  r_core = inverseTransform(vc, rmn_core)
  z_core = inverseTransform(vc, zmn_core)

  #line from core to edge
  r_plasma = r_boundary - r_core
  z_plasma = z_boundary - z_core

  #line from core to desired point
  r_point = x.r - r_core
  z_point = x.z - z_core

  #calculate the thetas
  θ_plasma = atan(z_plasma, r_plasma)
  θ_point = atan(z_point, r_point)

  θguess = θ_point - θ_plasma

  #get new boundary points
  vc = VmecCoordinates(0.5, θguess, ζ)
  r_boundary = inverseTransform(vc, rmn_boundary)
  z_boundary = inverseTransform(vc, zmn_boundary)

  #we now calculate r/a^2 = s for out point
  a = (r_boundary - r_core)^2 + (z_boundary - z_core)^2
  r = (x.r - r_core)^2 + (x.z - z_core)^2
  sguess = r/a

  if sguess > 1.0
    sguess = 1.0
  elseif sguess < 0.0
    sguess = 0.0
  end

  return sguess, θguess
  
end



function transform_deriv(::CylindricalFromFlux,
                         x::FluxCoordinates,
                         vmecsurf::VmecSurface;
                        )
  v = VmecFromFlux()(x,vmecsurf)
  dV = derivatives(v,vmecsurf)
  dsdψ = 2π/vmecsurf.phi[end]*vmecsurf.signgs
  dΛdθv = dV[2,2]
  dΛdζv = dV[2,3]

  dRdψ = dV[1,1]*dsdψ
  dZdψ = dV[3,1]*dsdψ
  dϕdψ = zero(typeof(x.θ))

  dRdθ = dV[1,2]/(1+dΛdθv)
  dZdθ = dV[3,2]/(1+dΛdθv)
  dϕdθ = zero(typeof(x.θ))

  dRdζ = -dV[1,3] + dV[1,2]*dΛdζv/(1+dΛdθv)
  dZdζ = -dV[3,3] + dV[3,2]*dΛdζv/(1+dΛdθv)
  dϕdζ = one(typeof(x.θ))
  return @SMatrix [dRdψ dRdθ dRdζ;
                   dϕdψ dϕdθ dϕdζ;
                   dZdψ dZdθ dZdζ]
end

function transform_deriv(::CylindricalFromVmec,
                         x::VmecCoordinates,
                         vmecsurf::VmecSurface;
                        )
  dRds = inverseTransform(x,vmecsurf.rmn;deriv=:ds)
  dZds = inverseTransform(x,vmecsurf.zmn;deriv=:ds)
  dϕds = zero(typeof(x.θ))

  dRdθ = inverseTransform(x,vmecsurf.rmn;deriv=:dθ)
  dZdθ = inverseTransform(x,vmecsurf.zmn;deriv=:dθ)
  dϕdθ = zero(typeof(x.θ))

  dRdζ = inverseTransform(x,vmecsurf.rmn;deriv=:dζ)
  dZdζ = inverseTransform(x,vmecsurf.zmn;deriv=:dζ)
  dϕdζ = one(typeof(x.θ))
  return @SMatrix [dRds dRdθ dRdζ;
                   dϕds dϕdθ dϕdζ;
                   dZds dZdθ dZdζ]
end

function derivatives(x::VmecCoordinates, vmecsurf::VmecSurface)
  dRds = inverseTransform(x,vmecsurf.rmn;deriv=:ds)
  dZds = inverseTransform(x,vmecsurf.zmn;deriv=:ds)
  dΛds = inverseTransform(x,vmecsurf.λmn;deriv=:ds)

  dRdθ = inverseTransform(x,vmecsurf.rmn;deriv=:dθ)
  dZdθ = inverseTransform(x,vmecsurf.zmn;deriv=:dθ)
  dΛdθ = inverseTransform(x,vmecsurf.λmn;deriv=:dθ)

  dRdζ = inverseTransform(x,vmecsurf.rmn;deriv=:dζ)
  dZdζ = inverseTransform(x,vmecsurf.zmn;deriv=:dζ)
  dΛdζ = inverseTransform(x,vmecsurf.λmn;deriv=:dζ)
  return @SMatrix [dRds dRdθ dRdζ;
                   dΛds dΛdθ dΛdζ;
                   dZds dZdθ dZdζ]
end

function derivatives(x::VmecCoordinates, vmecsurf::VmecSurface, field::Symbol)
  ds = inverseTransform(x,getfield(vmecsurf,field);deriv=:ds)
  dθ = inverseTransform(x,getfield(vmecsurf,field);deriv=:dθ)
  dζ = inverseTransform(x,getfield(vmecsurf,field);deriv=:dζ)
  return @SVector [ds, dθ, dζ]
end

#function transform_deriv(::CartesianFromVmec, x::VmecCoordinates, vmecsurf::VmecSurface)
#  y = transform_deriv(CylindricalFromVmec(),x,vmecsurf)
#  return transform_deriv(CartesianFromCylindrical(),y)
#end

function PlasmaEquilibriumToolkit.basis_vectors(::Covariant,
                                                ::CartesianFromVmec,
                                                x::VmecCoordinates,
                                                vmecsurf::VmecSurface
                                               )
  c = CylindricalFromVmec()(x,vmecsurf)
  d = transform_deriv(CylindricalFromVmec(),x,vmecsurf)
  sζ, cζ = sincos(x.ζ)
  dXds_x = d[1,1]*cζ
  dXds_y = d[1,1]*sζ
  dXds_z = d[3,1]

  dXdθ_x = d[1,2]*cζ
  dXdθ_y = d[1,2]*sζ
  dXdθ_z = d[3,2]

  dXdζ_x = d[1,3]*cζ - c.r*sζ
  dXdζ_y = d[1,3]*sζ + c.r*cζ
  dXdζ_z = d[3,3]

  return @SMatrix [dXds_x dXdθ_x dXdζ_x;
                   dXds_y dXdθ_y dXdζ_y;
                   dXds_z dXdθ_z dXdζ_z]
end

function PlasmaEquilibriumToolkit.basis_vectors(::Contravariant,
                                                ::CartesianFromVmec,
                                                x::VmecCoordinates,
                                                vmecsurf::VmecSurface;
                                               )
  g = inverseTransform(x,vmecsurf.gmn)
  return transform_basis(ContravariantFromCovariant(),basis_vectors(Covariant(),CartesianFromVmec(),x,vmecsurf),g)
end

function PlasmaEquilibriumToolkit.basis_vectors(::Covariant,
                                                ::CartesianFromPest,
                                                x::PestCoordinates,
                                                vmecsurf::VmecSurface;
                                               )
  contravariantPestBasis = basis_vectors(Contravariant(),CartesianFromPest(),x,vmecsurf)
  return transform_basis(CovariantFromContravariant(),contravariantPestBasis)
end

function PlasmaEquilibriumToolkit.basis_vectors(::Contravariant,
                                                ::CartesianFromPest,
                                                x::PestCoordinates,
                                                vmecsurf::VmecSurface;
                                               )
  v = VmecFromPest()(x,vmecsurf)
  contravariantVmecBasis = basis_vectors(Contravariant(),CartesianFromVmec(),v,vmecsurf)
  return transform_basis(PestFromVmec(),v,contravariantVmecBasis,vmecsurf)
end

function PlasmaEquilibriumToolkit.basis_vectors(::Contravariant,
                                                ::CartesianFromPesp,
                                                x::PespCoordinates,
                                                vmecsurf::VmecSurface;
                                               )
  v = VmecFromPesp()(x,vmecsurf)
  contravariantVmecBasis = basis_vectors(Contravariant(),CartesianFromVmec(),v,vmecsurf)
  return transform_basis(PespFromVmec(),v,contravariantVmecBasis,vmecsurf)
end

function PlasmaEquilibriumToolkit.basis_vectors(::Covariant,
                                                ::CartesianFromFlux,
                                                x::FluxCoordinates,
                                                vmecsurf::VmecSurface;
                                               )
  contravariantFluxBasis = basis_vectors(Contravariant(),CartesianFromFlux(),x,vmecsurf)
  return transform_basis(CovariantFromContravariant(),contravariantFluxBasis)
end

function PlasmaEquilibriumToolkit.basis_vectors(::Contravariant,
                                                ::CartesianFromFlux,
                                                x::FluxCoordinates,
                                                vmecsurf::VmecSurface;
                                               )
  v = VmecFromFlux()(x,vmecsurf)
  contravariantVmecBasis = basis_vectors(Contravariant(),CartesianFromVmec(),v,vmecsurf)
  return transform_basis(FluxFromVmec(),v,contravariantVmecBasis,vmecsurf)
end

# The transform_basis functions need to take into account the definitions of rotational
# transform in the respective coordinate systems
# αₚ = θₚ - ιₚζₚ = θᵥ + λ(s,θᵥ,ϕᵥ) - (-ιᵥ)(-ϕᵥ)
# => ∇αₚ = ∇θᵥ + dλ/ds ∇s + dλ/dθᵥ ∇θᵥ + dλ/dϕᵥ ∇ϕᵥ - dιᵥ/ds ϕᵥ ∇s - ιᵥ ∇ϕᵥ
# => ∇αₚ = (dλ/ds - dιᵥ/ds ϕᵥ)∇s+ (1 + dλ/dθᵥ)∇θᵥ + (dλ/dϕᵥ - ιᵥ)∇ϕᵥ
function PlasmaEquilibriumToolkit.transform_basis(::PestFromVmec,
                                                  v::VmecCoordinates,
                                                  e::BasisVectors,
                                                  vmecsurf::VmecSurface;
                                                 )
  d = derivatives(v,vmecsurf,:λmn)
  Φ = vmecsurf.phi[end]/(2π)*vmecsurf.signgs
  ∇ψ = Φ*e[:,1]
  ∇ζ = -e[:,3]
  ∇α = sign(Φ)*(e[:,1]*(d[1] - vmecsurf.iota[2]*v.ζ)
                + e[:,2]*(d[2] + 1) +
                e[:,3]*(d[3] - vmecsurf.iota[1]))
  return hcat(∇ψ, ∇α, ∇ζ)
end

function PlasmaEquilibriumToolkit.transform_basis(::PespFromVmec,
                                                  v::VmecCoordinates,
                                                  e::BasisVectors,
                                                  vmecsurf::VmecSurface;
                                                 )
  d = derivatives(v,vmecsurf,:λmn)
  Φ = vmecsurf.chi[end]/(2π)*vmecsurf.signgs
  ∇ψ = Φ*e[:,1]
  ∇ζ = -e[:,3]
  ∇α = sign(Φ)*(e[:,1]*(d[1] - vmecsurf.iota[2]*v.ζ)
                + e[:,2]*(d[2] + 1) +
                e[:,3]*(d[3] - vmecsurf.iota[1]))
  return hcat(∇ψ, ∇α, ∇ζ)
end

# => ∇θ = ∇θᵥ + dλ/ds ∇s + dλ/dθᵥ ∇θᵥ + dλ/dϕᵥ ∇ϕᵥ
function PlasmaEquilibriumToolkit.transform_basis(::FluxFromVmec,
                                                  v::VmecCoordinates,
                                                  e::BasisVectors,
                                                  vmecsurf::VmecSurface;
                                                 )
  d = derivatives(v,vmecsurf,:λmn)
  Φ = vmecsurf.phi[end]/(2π)*vmecsurf.signgs
  ∇ψ = Φ*e[:,1]
  ∇ζ = -e[:,3]
  ∇θ = sign(Φ)*(e[:,1]*d[1] + e[:,2]*(1 + d[2]) + e[:,3]*d[3])
  return hcat(∇ψ, ∇θ, ∇ζ)
end

# Transform coordinates to the GENE coordinate system
# x = sqrt(ψ*2π/ψ_lcfs)
# ∇x = ∇ψ dx/dψ = ∇ψ 1/(2 x) 2π / ψ_lcfs
# y = x * αₚ
# ∇y = ∇(αₚ/ι)|ι₀|x₀
#    = x(∇αₚ + αₚ * 2π/ψ_lcfs * ι'/ι * ∇ψ)
# z = αₚ - ι * ζ
# ∇z = ∇αₚ - ∇ι * ζ - ι * ∇ζ
#    = ∇αₚ - 2π/ψ_lcfs * ι' * ∇ψ * ζ - ι * ∇ζ
function PlasmaEquilibriumToolkit.transform_basis(::GeneFromPest,
                                                  p::PestCoordinates,
                                                  e::BasisVectors,
                                                  vmecsurf::VmecSurface;
                                                 )
  Aminor = vmecsurf.Aminor_p
  Φ = vmecsurf.phi[end]*vmecsurf.signgs
  x = sqrt(p.ψ*2π/Φ)
  ∇x = Aminor*π/(Φ*x)*e[:,1]
  # Multiple by sign(Φ) to ensure coordinate system is left handed
  ∇y = sign(Φ) * Aminor * x * (e[:,2] + p.α/vmecsurf.iota[1] * vmecsurf.iota[2] * 2π/Φ * e[:, 1])
  ∇z = Aminor*(e[:,2] - p.ζ*vmecsurf.iota[2]*2π/Φ*e[:,1] - vmecsurf.iota[1]*e[:,3])
  return hcat(∇x, ∇y, ∇z)
end

# ϕ: toroidal flux as a function of poloidal pest ψ
# ϕ(ψ*2π/ψ_lcfs)
# x = sqrt(ϕ(ψ*2π/ψ_lcfs)/ϕ_lcfs)
# ∇x = ∇ψ dx/dψ = ∇ψ π ϕ' / (ψ_lcfs * ϕ_lcfs * x)
# y = x * αₚ
# ∇y = ∇(αₚ/ι)|ι₀|x₀
#    = x(∇αₚ + αₚ * 2π/ψ_lcfs * ι'/ι * ∇ψ)
# z = αₚ - ι * ζ
# ∇z = ∇αₚ - ∇ι * ζ - ι * ∇ζ
#    = ∇αₚ - 2π/ψ_lcfs * ι' * ∇ψ * ζ - ι * ∇ζ
function PlasmaEquilibriumToolkit.transform_basis(::GeneFromPesp,
                                                  p::PespCoordinates,
                                                  e::BasisVectors,
                                                  vmecsurf::VmecSurface;
                                                 )
  Aminor = vmecsurf.Aminor_p
  χ = vmecsurf.chi[end]*vmecsurf.signgs
  x = sqrt(vmecsurf.phi[1]/vmecsurf.phi[end])
  ∇x = Aminor*π*vmecsurf.phi[2] / (χ * vmecsurf.phi[end] * x)*e[:,1]
  # Multiple by sign(ξ) to ensure coordinate system is left handed
  ∇y = sign(χ) * Aminor * x * (e[:,2] + p.α/vmecsurf.iota[1] * vmecsurf.iota[2] * 2π/χ * e[:, 1])
  ∇z = Aminor*(e[:,2] - p.ζ*vmecsurf.iota[2]*2π/χ*e[:,1] - vmecsurf.iota[1]*e[:,3])
  return hcat(∇x, ∇y, ∇z)
end


# Replicate a GIST file
function geneGeometryCoefficients(::GeneFromPest,
                                  p::PestCoordinates,
                                  vmecsurf::VmecSurface;
                                 )
  Aminor = vmecsurf.Aminor_p
  Ba = abs(vmecsurf.phi[end])/(π*Aminor^2)
  v = VmecFromPest()(p,vmecsurf)
  vmecContraBasis = basis_vectors(Contravariant(), CartesianFromVmec(), v, vmecsurf)
  pestContraBasis = transform_basis(PestFromVmec(), v, vmecContraBasis,vmecsurf)
  geneContraBasis = transform_basis(GeneFromPest(), p, pestContraBasis,vmecsurf)
  geneCoBasis = transform_basis(CovariantFromContravariant(), geneContraBasis)
  gB = grad_B(v, vmecContraBasis, vmecsurf)
  geneMetric = metric(geneContraBasis)
  BnormMag = norm(cross(geneContraBasis[:,1], geneContraBasis[:,2]))
  # The Jacobian should always be negative, but as only the integral measure
  # is required, that must be positive negative
  jac = abs(1.0/dot(geneContraBasis[:,1], cross(geneContraBasis[:,2], geneContraBasis[:,3])))
  K1, K2 = Aminor/Ba.*grad_B_projection(geneContraBasis,gB)
  dBdZ = Aminor/Ba*dot(gB, geneCoBasis[:,3])
  # The GENE K1 component is the negative of what is computed, see the GIST documentation note
  return geneMetric, BnormMag, abs(jac), -K1, K2, dBdZ
end

function geneGeometryCoefficients(::GeneFromPesp,
  p::PespCoordinates,
  vmecsurf::VmecSurface;
 )
  Aminor = vmecsurf.Aminor_p
  Ba = abs(vmecsurf.phi[end])/(π*Aminor^2)
  v = VmecFromPesp()(p,vmecsurf)
  vmecContraBasis = basis_vectors(Contravariant(), CartesianFromVmec(), v, vmecsurf)
  pespContraBasis = transform_basis(PespFromVmec(), v, vmecContraBasis,vmecsurf)
  geneContraBasis = transform_basis(GeneFromPesp(), p, pespContraBasis,vmecsurf)
  geneCoBasis = transform_basis(CovariantFromContravariant(), geneContraBasis)
  gB = grad_B(v, vmecContraBasis, vmecsurf)
  geneMetric = metric(geneContraBasis)
  BnormMag = norm(cross(geneContraBasis[:,1], geneContraBasis[:,2]))
  # The Jacobian should always be negative, but as only the integral measure
  # is required, that must be positive negative
  jac = abs(1.0/dot(geneContraBasis[:,1], cross(geneContraBasis[:,2], geneContraBasis[:,3])))
  K1, K2 = Aminor/Ba.*grad_B_projection(geneContraBasis,gB)
  dBdZ = Aminor/Ba*dot(gB, geneCoBasis[:,3])
  # The GENE K1 component is the negative of what is computed, see the GIST documentation note
  return geneMetric, BnormMag, abs(jac), -K1, K2, dBdZ
end

function geneGeometryCoefficients(::GeneFromPest,
                                  p::StructArray{PestCoordinates{T,T}},
                                  vmecsurf::VmecSurface
                                 ) where {T}
  geneMetric = Array{SVector{6,Float64}}(undef, size(p))
  K1 = Array{Float64}(undef, size(p))
  K2 = similar(K1)
  dBdZ = similar(K1)
  BnormMag = similar(K1)
  absJac = similar(K1)
  @batch for i in eachindex(p)
    geneMetric[i], BnormMag[i], absJac[i], K1[i], K2[i], dBdZ[i] = geneGeometryCoefficients(GeneFromPest(),p[i],vmecsurf)
  end
  return geneMetric, BnormMag, absJac, K1, K2, dBdZ
end

function geneGeometryCoefficients(::GeneFromPesp,
                                  p::StructArray{PespCoordinates{T,T}},
                                  vmecsurf::VmecSurface
                                 ) where {T}
  geneMetric = Array{SVector{6,Float64}}(undef, size(p))
  K1 = Array{Float64}(undef, size(p))
  K2 = similar(K1)
  dBdZ = similar(K1)
  BnormMag = similar(K1)
  absJac = similar(K1)
  @batch for i in eachindex(p)
    geneMetric[i], BnormMag[i], absJac[i], K1[i], K2[i], dBdZ[i] = geneGeometryCoefficients(GeneFromPesp(),p[i],vmecsurf)
  end
  return geneMetric, BnormMag, absJac, K1, K2, dBdZ
end

function geneGeometryCoefficientsPol(::GeneFromPest,
                                     p::StructArray{PestCoordinates{T,T}},
                                     vmecsurf::VmecSurface,
                                     vmec::Vmec
                                    ) where {T}
  geneMetric = Array{SVector{6,Float64}}(undef, size(p))
  K1 = Array{Float64}(undef, size(p))
  K2 = similar(K1)
  dBdZ = similar(K1)
  BnormMag = similar(K1)
  absJac = similar(K1)
  @batch for i in eachindex(p)
    geneMetric[i], BnormMag[i], absJac[i], K1[i], K2[i], dBdZ[i] = geneGeometryCoefficientsPol(GeneFromPest(),p[i],vmecsurf,vmec)
  end
  return geneMetric, BnormMag, absJac, K1, K2, dBdZ
end

# Compute the normalized global magnetic shear
function shat(vmecsurf::VmecSurface)
  return -2*vmecsurf.s*vmecsurf.iota[2]/vmecsurf.iota[1]
end
