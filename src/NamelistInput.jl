
function parse_namelist_array(::Type{T},
                              s::AbstractString;
                             ) where T <: Array{DT,1} where {DT <: Real}
  return parse.(DT, strip.(split(s)))
end

function parse_namelist_bool(s::AbstractString)
  if s == "F" || s == "f" || s == ".F." || s == ".f." || s == ".FALSE." || s == ".false."
    return false
  elseif s == "T" || s == "t" || s == ".T." || s == ".t." || s == ".TRUE." || s == ".true."
    return true
  else
    throw(ArgumentError("Could not parse fortran logical"))
  end
end

"""
    read_vmec_namelist(nml_file::AbstractString)

Reads a VMEC input file in Fortran namelist format and populates a corresponding `Dict{Symbol,Any}`,
where the `Symbol` key maps directly to the variable label in the namelist file.  Values the type of
the input variable is defined in the global VmecInputTypes `Dict`.
"""
function read_vmec_namelist(nml_file::AbstractString)
  file_stream = open(nml_file)
  nml_string = read(file_stream, String)
  close(file_stream)

  # Replace &INDATA and /
  nml_string = replace(nml_string, "&INDATA\n"=>"")
  nml_string = replace(nml_string, "/\n"=>"")

  # Filter the comments
  comment_regex = r"![^\n]*"
  nml_string = replace(nml_string, comment_regex=>"")
    
  space_matches = collect(eachmatch(r"(?<num>[0-9])\s+(?<letter>[a-zA-Z])", nml_string))
  nml_array = [nml_string...]
  if !isempty(space_matches)
    offset = 0
    for s in space_matches
      if !occursin("\n", s.match)
        insert!(nml_array, s.offset + 1 + offset, '\n')
        offset += 1
      end
    end
  end

  nml_string = string(nml_array...)

  # Filter any entries where the data begins on the next line
  nml_string = replace(nml_string, r"=\s*\n\s*"=>"=")
  nml_string = replace(nml_string, r"\n{2,}" => "\n")


  # Find locations where the a newline character interrupts data
  newline_array_locs = findall(r"[0-9]\s*\n\s*[0-9]", nml_string)
  while !isempty(newline_array_locs)
    if !isempty(newline_array_locs)
      for r in newline_array_locs
        nml_string = nml_string[1:r[1]]*" "*nml_string[r[end]:end]
      end
    end
    newline_array_locs = findall(r"[0-9]\s*\n\s*[0-9]", nml_string)
  end

  # Split on new line characters
  nml_line_splits = split(nml_string, '\n')
nml_lines = Vector{SubString{String}}(undef, 0)

  # Split on any commas
  comma_regex = r"([_*a-zA-Z0-9_*]*\(*\s*\-*[0-9]*,*\s*\-*[0-9]*\)*\s*=\s*[^,]*)"
  for (j, line) in enumerate(nml_line_splits)
    if !isempty(line)
      matches = collect(eachmatch(comma_regex, line))
      if !isempty(matches)
        push!(nml_lines, strip(matches[1].captures[1]))
        if length(matches) > 1
          for i in 2:length(matches)
            push!(nml_lines, strip(matches[i].captures[1]))
          end
        end
      else
        push!(nml_lines, strip(line))
      end
    end
  end

  nml_dict = OrderedDict{Symbol,Any}()
  for (i, line) in enumerate(nml_lines)
    @debug "line $i: $line"
    line1 = replace(line, r"\(\s*"=>"(", r",\s*"=>",")
    line2 = replace(line1,"="=>" = ")
    matches = collect(eachmatch(r"([^\s]+)", line2))
    @debug "matches = $matches, length(matches) = $(length(matches))"
    length(matches) >= 3 || error("Error parsing $(line2) in read_vmec_namelist, not a balanced assignment statement")
    key = Symbol(lowercase(matches[1].captures[1]))
    @debug "Key: $key"
    value_str = ""
    if length(matches) > 3
      value_str *= "["*sanitize_fortran_input(matches[3].captures[1])
      for j = 4:length(matches)
        value_str *= ","*sanitize_fortran_input(matches[j].captures[1])
      end
      value_str *= "]"
    else
      value_str *= sanitize_fortran_input(matches[3].captures[1])
    end
    @debug "Value string: $value_str"
    nml_dict[key] = eval(Meta.parse(value_str))
  end
  return nml_dict
end

"""
    sanitizeFortanInput(s::AbstractString)

Return the correct julia type to be used in Meta.parse
"""
function sanitize_fortran_input(s::AbstractString)
  if lowercase(s) == "f" || lowercase(s) == ".false." || lowercase(s) == ".f."
    return "false"
  elseif lowercase(s) == "t" || lowercase(s) == ".true." || lowercase(s) == ".t."
    return "true"
  elseif occursin("'", s)
    return replace(s, "\'"=>"\"")
  else
    return s
  end
end

"""
    update_input_namelist(nml_dict::AbstractDict{Symbol,Any},vmec::VmecData)

Construct a new dictionary from `nml_dict` with entries updated by the
associated values from `vmec`.

See also: [`update_input_namelist!`](@ref)
"""
function update_input_namelist(nml_dict::AbstractDict{Symbol,Any},
                               vmec::VmecData;
                              )
  newDict = copy(nml_dict)
  update_input_namelist!(newDict,vmec)
  return newDict
end

"""
    update_input_namelist!(nml_dict::AbstractDict{Symbol,Any},vmec::VmecData)

Perform an in-place update of the entires of `nml_dict` from the associated
values in `vmec`.

See also: [`update_input_namelist`](@ref)
"""
function update_input_namelist!(nml_dict::AbstractDict{Symbol,Any},
                                vmec::VmecData{T};
                               ) where {T}
  nml_dict[:mpol] == vmec.mpol || error("Number of poloidal modes must match; old eq: $(nml_dict[:mpol]), new eq: $(vmec.mpol)")
  nml_dict[:ntor] == vmec.ntor || error("Number of toroidal modes must match; old eq: $(nml_dict[:ntor]), new eq: $(vmec.ntor)")
  nml_dict[:nfp] == vmec.nfp || error("Number of field periods must match; old eq: $(nml_dict[:nfp]), new eq: $(vmec.nfp)")
  # Deal with any quantity that has a one-to-one
  # correspondence with the input dictionary
  for key in propertynames(vmec)
    if haskey(VmecInputTypes,key) && isdefined(vmec, key)
      value = getfield(vmec,key)
      input_type = VmecInputTypes[key]
      typeof(getfield(vmec, key)) <: input_type || throw(error("Incorrect type for VMEC namelist parameter $key"))
      nml_dict[key] = value
    end
  end
  # Update the axis and delete the legacy :raxis and :zaxis if necessary
  if haskey(nml_dict, :raxis)
    if !haskey(nml_dict, :raxis_cc)
      typeof(getfield(vmec, :raxis_cc)) <: VmecInputType[:raxis_cc] || throw(error("Incorrect type for VMEC namelist parameter raxis_cc"))
      nml_dict[:raxis_cc] = getfield(vmec, :raxis_cc)
    end
    if !haskey(nml_dict, :raxis_cs)
      typeof(getfield(vmec, :raxis_cs)) <: VmecInputType[:raxis_cs] || throw(error("Incorrect type for VMEC namelist parameter raxis_cs"))
      nml_dict[:raxis_cs] = getfield(vmec, :raxis_cs)
    end
    delete!(nml_dict, :raxis)
  end
  if haskey(nml_dict, :zaxis)
    if !haskey(nml_dict, :zaxis_cc)
      typeof(getfield(vmec, :zaxis_cc)) <: VmecInputType[:zaxis_cc] || throw(error("Incorrect type for VMEC namelist parameter zaxis_cc"))
      nml_dict[:zaxis_cc] = getfield(vmec, :zaxis_cc)
    end
    if !haskey(nml_dict, :zaxis_cs)
      typeof(getfield(vmec,:zaxis_cs)) <: VmecInputType[:zaxis_cs] || throw(error("Incorrect type for VMEC namelist parameter zaxis_cs"))
      nml_dict[:zaxis_cs] = getfield(vmec, :zaxis_cs)
    end
    delete!(nml_dict, :zaxis)
  end
  ntor = vmec.ntor
  mpol = vmec.mpol
  nfp = vmec.nfp
  ns = vmec.ns
  i = 0
  for n = 0:ntor
    i += 1
    sym = Symbol("rbc("*string(n)*",0)")
    nml_dict[sym] = vmec.rmnc[i,ns]
    sym = Symbol("zbs("*string(n)*",0)")
    nml_dict[sym] = vmec.zmns[i,ns]
    if nml_dict[:lasym]
      sym = Symbol("rbs("*string(n)*",0)")
      nml_dict[sym] = vmec.rmns[i,ns]
      sym = Symbol("zbc("*string(n)*",0)")
      nml_dict[sym] = vmec.zmnc[i,ns]
    end
  end
  for m = 1:mpol-1
    for n = -ntor:ntor
      i += 1
      sym = Symbol("rbc("*string(n)*","*string(m)*")")
      nml_dict[sym] = vmec.rmnc[i,ns]
      sym = Symbol("zbs("*string(n)*","*string(m)*")")
      nml_dict[sym] = vmec.zmns[i,ns]
      if nml_dict[:lasym]
        sym = Symbol("rbs("*string(n)*","*string(m)*")")
        nml_dict[sym] = vmec.rmns[i,ns]
        sym = Symbol("zbc("*string(n)*","*string(m)*")")
        nml_dict[sym] = vmec.zmnc[i,ns]
      end
    end
  end
end

"""
    VmecInputTypes <: Dict{Symbol,Type}

Dictionary containing the type information of VMEC input variables.
"""
VmecInputTypes = Dict{Symbol,Type}()

VmecInputTypes[:lasym] = Bool
VmecInputTypes[:lfreeb] = Bool
VmecInputTypes[:lwouttxt] = Bool
VmecInputTypes[:lpofr] = Bool
VmecInputTypes[:full3d1out] = Bool

VmecInputTypes[:ns_array] = Array{Int32,1}
VmecInputTypes[:niter_array] = Array{Int32,1}
VmecInputTypes[:niter] = Int32
VmecInputTypes[:nfp] = Int32
VmecInputTypes[:nstep] = Int32
VmecInputTypes[:mpol] = Int32
VmecInputTypes[:ntor] = Int32
VmecInputTypes[:ncurr] = Int32
VmecInputTypes[:nzeta] = Int32
VmecInputTypes[:nvacskip] = Int32
VmecInputTypes[:nsin] = Int32
VmecInputTypes[:ntheta] = Int32
VmecInputTypes[:mfilter_fbdy] = Int32
VmecInputTypes[:nfilter_fbdy] = Int32
VmecInputTypes[:max_main_iterations] = Int32
VmecInputTypes[:omp_num_threads] = Int32

VmecInputTypes[:ftol_array] = Array{Float64,1}
VmecInputTypes[:ai] = Array{Float64,1}
VmecInputTypes[:am] = Array{Float64,1}
VmecInputTypes[:ac] = Array{Float64,1}
VmecInputTypes[:prec2d_threshold] = Float64
VmecInputTypes[:delt] = Float64
VmecInputTypes[:tcon0] = Float64
VmecInputTypes[:phiedge] = Float64
VmecInputTypes[:extcur] = Float64
VmecInputTypes[:gamma] = Float64
VmecInputTypes[:bloat] = Float64
VmecInputTypes[:spres_ped] = Float64
VmecInputTypes[:pres_scale] = Float64
VmecInputTypes[:curtor] = Float64
VmecInputTypes[:raxis] = Array{Float64,1}
VmecInputTypes[:zaxis] = Array{Float64,1}
VmecInputTypes[:raxis_cc] = Array{Float64,1}
VmecInputTypes[:raxis_cs] = Array{Float64,1}
VmecInputTypes[:zaxis_cc] = Array{Float64,1}
VmecInputTypes[:zaxis_cs] = Array{Float64,1}
VmecInputTypes[:am_aux_s] = Array{Float64,1}
VmecInputTypes[:am_aux_f] = Array{Float64,1}
VmecInputTypes[:ai_aux_s] = Array{Float64,1}
VmecInputTypes[:ac_aux_s] = Array{Float64,1}
VmecInputTypes[:ac_aux_f] = Array{Float64,1}
VmecInputTypes[:aphi] = Array{Float64,1}

VmecInputTypes[:precon_type] = String
VmecInputTypes[:mgrid_file] = String
VmecInputTypes[:pmass_type] = String
VmecInputTypes[:pcurr_type] = String
VmecInputTypes[:piota_type] = String

#Populate the coefficient symbols
for m = 0:32
  for n = -32:32
    sym = Symbol("rbc("*string(n)*","*string(m)*")")
    VmecInputTypes[sym] = Float64
    sym = Symbol("rbs("*string(n)*","*string(m)*")")
    VmecInputTypes[sym] = Float64
    sym = Symbol("zbc("*string(n)*","*string(m)*")")
    VmecInputTypes[sym] = Float64
    sym = Symbol("zbs("*string(n)*","*string(m)*")")
    VmecInputTypes[sym] = Float64
  end
end
